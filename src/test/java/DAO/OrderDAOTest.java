package DAO;

import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Orders;
import com.my.project.rent.db.entity.objects.Order;
import com.my.project.rent.db.entity.objects.OrderCarBean;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class OrderDAOTest {
    @Test
    public void testAllMethods(){
        Orders orderDAO = DAOFactory.getDAOFactory().getOrderDAO();
        List<OrderCarBean> orders = null;
        List<OrderCarBean> orders2 = null;
        Order order = null;
        try{
            order = orderDAO.getOrderById(1);
            orderDAO.updateStatus(order);
            orderDAO.updateMessage(order);
            orderDAO.updateRepair(order);
            orders = orderDAO.getAllOrders();
            orders2 = orderDAO.getOrdersByUserId(1);
        }catch (SQLException e){
            e.printStackTrace();
        }
        Assert.assertNotNull(orders);
        Assert.assertNotNull(orders2);
    }
}
