package DAO;

import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.entity.objects.Car;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class CarDAOTest{
    @Test
    public void testAllMethods() throws SQLException {
        Cars carDAO = DAOFactory.getDAOFactory().getCarDAO();
        Car test = carDAO.getCarById(1);
        carDAO.updateCarStatus(test);
        carDAO.update(test);
        List<Car> cars = carDAO.getAllCars(2);
        List<Car> cars2 = carDAO.getDeletedCars();
        List<Car> cars3 = carDAO.getCarsByPrice(2);
        List<Car> cars4 = carDAO.searchCars(2, "BMW X5");
        List<Car> cars5 = carDAO.searchSecond(2, "BMW X5");
        List<Car> cars6 = carDAO.searchCarsByBrandOrClass(2, "BMW");
        int count = carDAO.getCount();
        Assert.assertEquals(cars.get(0).getBrand(), "BMW");
        Assert.assertEquals(cars2.size(), 0);
        Assert.assertEquals(cars3.get(0).getBrand(), "Volkswagen");
        Assert.assertEquals(cars4.size(), 0);
        Assert.assertEquals(cars5.size(), 1);
        Assert.assertEquals(cars6.size(), 2);
        Assert.assertEquals(test.getBrand(), "BMW");

        Assert.assertEquals(count, 10);

    }
}