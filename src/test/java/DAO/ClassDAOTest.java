package DAO;

import com.my.project.rent.db.dao.interfaces.Classes;
import com.my.project.rent.db.dao.sql.FactoryDAO;
import com.my.project.rent.db.entity.objects.CarClass;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class ClassDAOTest {
    @Test
    public void testAllMethods() throws SQLException {
        Classes classDAO = FactoryDAO.getDAOFactory().getClassDAO();
        List<CarClass> classes = classDAO.getAllClasses();
        Assert.assertEquals(classes.size(), 9);
    }
}
