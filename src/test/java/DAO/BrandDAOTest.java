package DAO;

import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Brands;
import com.my.project.rent.db.entity.objects.Brand;
import org.junit.Assert;
import org.junit.Test;
import java.sql.SQLException;
import java.util.List;

public class BrandDAOTest {
    @Test
    public void testAllMethods(){
        Brands brandDAO = DAOFactory.getDAOFactory().getBrandDAO();
        List<Brand> brands = null;
        try {
            brandDAO.insert(new Brand());
            brands = brandDAO.getAllBrands();
        } catch (SQLException e){
            e.printStackTrace();
        }

        Assert.assertEquals(brands.get(0).getName(), "BMW");
    }
}
