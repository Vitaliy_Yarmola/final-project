package DAO;

import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Brands;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.Brand;
import com.my.project.rent.db.entity.objects.User;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;

public class UserDAOTest {
    @Test
    public void testAllMethods(){
        Users userDAO = DAOFactory.getDAOFactory("mysql").getUserDAO();
        List<User> users = null;
        User test = null;
        User test2 = null;
        try {
            users = userDAO.getAllUsers();
            test = userDAO.getUserById(6);
            userDAO.update(test);
            userDAO.updateStatuses(test);
            test2 = userDAO.findUserByEmail(test.getEmail());
            Assert.assertTrue(userDAO.isUserEmailExist(test.getEmail()));
            Assert.assertEquals(test.getIdCode(), test2.getIdCode());
        } catch (SQLException e){
            e.printStackTrace();
        }
        Assert.assertNotNull(users);
    }
}
