package Entity;

import com.my.project.rent.db.entity.objects.Car;
import org.junit.Assert;
import org.junit.Test;

public class CarTest {
    @Test
    public void testAllMethods(){
        Car car = new Car();
        car.setId(11);
        car.setBrand("Toyota");
        car.setModel("Rav4");
        car.setUrl("picture");
        car.setCarcase("A-class");
        car.setColor("red");
        car.setCapacity(12d);
        car.setGearBox("Automatic");
        car.setFuel("Benzine");
        car.setPrice(2000d);
        car.setStatusId(0);
        Assert.assertEquals(11, car.getId());
        Assert.assertEquals("Toyota", car.getBrand());
        Assert.assertEquals("Rav4", car.getModel());
        Assert.assertEquals("picture", car.getUrl());
        Assert.assertEquals("A-class", car.getCarcase());
        Assert.assertEquals("red", car.getColor());
        Assert.assertEquals(java.util.Optional.of(12d), java.util.Optional.of(car.getCapacity()));
        Assert.assertEquals("Automatic", car.getGearBox());
        Assert.assertEquals("Benzine", car.getFuel());
        Assert.assertEquals(java.util.Optional.of(2000d), java.util.Optional.of(car.getPrice()));
        Assert.assertEquals(0, car.getStatusId());
    }
}
