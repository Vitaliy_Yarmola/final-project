package Entity;

import com.my.project.rent.db.entity.objects.Brand;
import com.my.project.rent.db.entity.objects.CarClass;
import org.junit.Assert;
import org.junit.Test;

public class ClassTest {
    @Test
    public void testAllMethods(){
        CarClass carClass = new CarClass();
        carClass.setId(11);
        carClass.setName("Z-class");
        Assert.assertEquals(11, carClass.getId());
        Assert.assertEquals("Z-class", carClass.getName());
    }
}
