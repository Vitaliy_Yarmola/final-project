package Entity;

import com.my.project.rent.db.entity.objects.OrderCarBean;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class OrderCarBeanTest {
    @Test
    public void testAllMethods(){
        OrderCarBean order = new OrderCarBean();
        order.setUserId(1);
        order.setCarId(1);
        order.setOrderId(1);
        order.setName("Nikolay");
        order.setSurname("Hammond");
        order.setUserStatus(0);
        order.setPhone("+380");
        order.setBrand("BMW");
        order.setModel("X5");
        order.setCost(2000d);
        order.setDriver(true);
        order.setStartRent(new Date(12202045L));
        order.setEndRent(new Date(12202090L));
        order.setMessage("qwerty");
        order.setOrderStatusId(6);
        order.setRepair(1200d);
        order.setDebtor(false);
        Assert.assertEquals(1, order.getUserId());
        Assert.assertEquals(1, order.getCarId());
        Assert.assertEquals(1, order.getOrderId());
        Assert.assertEquals(0, order.getUserStatus());
        Assert.assertEquals("Nikolay", order.getName());
        Assert.assertEquals("Hammond", order.getSurname());
        Assert.assertEquals("+380", order.getPhone());
        Assert.assertEquals("BMW", order.getBrand());
        Assert.assertEquals("X5", order.getModel());
        Assert.assertEquals(java.util.Optional.of(2000d), java.util.Optional.of(order.getCost()));
        Assert.assertTrue(order.isDriver());
        Assert.assertEquals(new Date(12202045L), order.getStartRent());
        Assert.assertEquals(new Date(12202090L), order.getEndRent());
        Assert.assertEquals("qwerty", order.getMessage());
        Assert.assertEquals(6, order.getOrderStatusId());
        Assert.assertEquals(java.util.Optional.of(1200d), java.util.Optional.of(order.getRepair()));
        Assert.assertFalse(order.isDebtor());
    }
}
