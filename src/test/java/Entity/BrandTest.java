package Entity;

import com.my.project.rent.db.entity.objects.Brand;
import com.my.project.rent.db.entity.objects.Car;
import org.junit.Assert;
import org.junit.Test;

public class BrandTest {
    @Test
    public void testAllMethods(){
        Brand brand = new Brand();
        brand.setId(11);
        brand.setName("Toyota");
        Assert.assertEquals(11, brand.getId());
        Assert.assertEquals("Toyota", brand.getName());
    }
}
