package Entity;

import com.my.project.rent.db.entity.objects.Order;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class OrderTest {
    @Test
    public void testAllMethods(){
        Order order = new Order();
        order.setId(1);
        order.setStatusId(1);
        order.setCost(2000d);
        order.setDriver(true);
        order.setPassportName("Nikolay");
        order.setPassportSurname("Hammond");
        order.setPhone("+380");
        order.setIdCode(120);
        order.setBeginRent(new Date(12202045L));
        order.setEndRent(new Date(12202090L));
        order.setBirth(new Date(12202090L));
        order.setMessage("qwerty");
        order.setUserId(2);
        order.setCarId(1);
        order.setRepair(1200d);
        Assert.assertEquals(1, order.getId());
        Assert.assertEquals(1, order.getStatusId());
        Assert.assertEquals(java.util.Optional.of(2000d), java.util.Optional.of(order.getCost()));
        Assert.assertTrue(order.isDriver());
        Assert.assertEquals("Nikolay", order.getPassportName());
        Assert.assertEquals("Hammond", order.getPassportSurname());
        Assert.assertEquals("+380", order.getPhone());
        Assert.assertEquals(120, order.getIdCode());
        Assert.assertEquals(new Date(12202045L), order.getBeginRent());
        Assert.assertEquals(new Date(12202090L), order.getEndRent());
        Assert.assertEquals(new Date(12202090L), order.getBirth());
        Assert.assertEquals("qwerty", order.getMessage());
        Assert.assertEquals(2, order.getUserId());
        Assert.assertEquals(1, order.getCarId());
        Assert.assertEquals(java.util.Optional.of(1200d), java.util.Optional.of(order.getRepair()));
    }
}
