package Entity;

import com.my.project.rent.db.entity.objects.Order;
import com.my.project.rent.db.entity.objects.User;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class UserTest {
    @Test
    public void testAllMethods(){
        User user = new User();
        user.setId(1);
        user.setName("Nikolay");
        user.setSurname("Hammond");
        user.setEmail("Hammond@gmail.com");
        user.setPassword("asd123");
        user.setPhone("+380");
        user.setRoleId(2);
        user.setStatusId(0);
        user.setIdCode(120);
        user.setDateBirth(new Date(12345678L));
        user.setDebtor(false);
        Assert.assertEquals(1, user.getId());
        Assert.assertEquals("Nikolay", user.getName());
        Assert.assertEquals("Hammond", user.getSurname());
        Assert.assertEquals("Hammond@gmail.com", user.getEmail());
        Assert.assertEquals("asd123", user.getPassword());
        Assert.assertEquals("+380", user.getPhone());
        Assert.assertEquals(2, user.getRoleId());
        Assert.assertEquals(0, user.getStatusId());
        Assert.assertEquals(120, user.getIdCode());
        Assert.assertEquals(new Date(12345678L), user.getDateBirth());
        Assert.assertFalse(user.isDebtor());

    }
}
