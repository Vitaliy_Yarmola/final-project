package Commands;

import com.my.project.rent.web.command.common.CarsCommand;
import org.junit.Assert;
import org.junit.Test;

public class ShowCarsTest {
    @Test
    public void testServlet(){
        Assert.assertEquals(10, CarsCommand.getAllCars(2).size());
        Assert.assertEquals(6, CarsCommand.getAllBrand().size());
        Assert.assertEquals(9, CarsCommand.getAllClasses().size());
    }
}