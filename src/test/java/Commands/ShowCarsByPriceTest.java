package Commands;

import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.web.command.sort.ShowCarsByPriceCommand;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ShowCarsByPriceTest {
    @Test
    public void testAllMethods(){
        List<Car> cars = ShowCarsByPriceCommand.getCarsByPrice(2);
        Assert.assertEquals(10, cars.size());
    }
}
