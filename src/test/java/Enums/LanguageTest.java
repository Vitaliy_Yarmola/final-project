package Enums;

import com.my.project.rent.db.entity.enums.Language;
import com.my.project.rent.db.entity.enums.Role;
import com.my.project.rent.db.entity.objects.User;
import org.junit.Assert;
import org.junit.Test;

public class LanguageTest {
    @Test
    public void testAllMethods(){
        User user = new User();
        user.setLanguageId(0);
        Assert.assertEquals("en", Language.getLanguage(user.getLanguageId()).getCode());
    }
}
