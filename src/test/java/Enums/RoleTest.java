package Enums;

import com.my.project.rent.db.entity.enums.Role;
import com.my.project.rent.db.entity.objects.User;
import org.junit.Assert;
import org.junit.Test;

public class RoleTest {
    @Test
    public void testAllMethods(){
        User user = new User();
        user.setRoleId(0);
        Assert.assertEquals("admin", Role.getRole(user).getName());
    }
}
