package com.my.project.rent.web.command;

import com.my.project.rent.web.command.admin.*;
import com.my.project.rent.web.command.common.*;
import com.my.project.rent.web.command.sort.*;
import com.my.project.rent.web.command.user.CreateOrderCommand;
import com.my.project.rent.web.command.user.OrdersUserCommand;

import java.util.Map;
import java.util.TreeMap;

public class CommandContainer {

	private static final Map<String, Command> commands = new TreeMap<>();
	
	static {
		// common commands
		commands.put("register", new RegisterCommand());
		commands.put("sign", new SignInCommand());
		commands.put("login", new LoginCommand());
		commands.put("main", new CarsCommand());
		commands.put("orders", new OrdersCommand());
		commands.put("create_order", new CreateOrderCommand());
		commands.put("edit_car", new EditCarCommand());
		commands.put("add_car", new AddNewCarCommand());
		commands.put("delete_car", new DeleteCommand());
		commands.put("log_out", new LogoutCommand());
		commands.put("profile", new ProfileCommand());
		commands.put("change_status", new ControlUsersCommand());
		commands.put("users", new UsersCommand());
		commands.put("admin_order", new OrdersAdminCommand());
		commands.put("client_order", new OrdersUserCommand());
		commands.put("sort_price", new ShowCarsByPriceCommand());
		commands.put("home", new GeneralCommand());
		commands.put("get_brand", new GetCarsByBrandsCommand());
		commands.put("get_class", new GetCarsByClassCommand());
		commands.put("search", new SearchCarsCommand());
		commands.put("get_deleted", new ShowDeletedCarsCommand());
		commands.put("add_manager", new AddManagerCommand());
		commands.put("noCommand", new NoSuchCommand());
		commands.put("change_lang", new ChangeLanguageCommand());
		commands.put("change_data", new EditProfileCommand());
		commands.put("change_password", new ChangePasswordCommand());
	}


	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			return commands.get("noCommand");
		}
		
		return commands.get(commandName);
	}
	
}