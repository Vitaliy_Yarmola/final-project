package com.my.project.rent.web.command.sort;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

public class GetCarsByClassCommand extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        User user = ((User) session.getAttribute("user"));
        int roleId = -1;
        if(user!=null) {
            roleId = user.getRoleId();
        }
        String forward = Path.PAGE_MAIN;
        Cars carDAO = DAOFactory.getDAOFactory().getCarDAO();
        String classes = request.getParameter("inputClasses");
        List<Car> cars = null;
        try {
            cars = carDAO.searchCarsByBrandOrClass(roleId, classes);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        session.setAttribute("cars", cars);
        return forward;
    }
}
