package com.my.project.rent.web.command.admin;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class EditCarCommand extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String forward = Path.PAGE_ERROR_PAGE;
        String errorMessage;
        String fileName = "";
        Car car = null;
        Cars carDAO =  DAOFactory.getDAOFactory().getCarDAO();
        int carId = Integer.parseInt(request.getParameter("car_number"));
        try {
            car = carDAO.getCarById(carId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        String carBrand = request.getParameter("brand");
        String carModel = request.getParameter("model");
        String carcase = request.getParameter("carcase");
        String carColor = request.getParameter("color");
        String carCapacity = request.getParameter("capacity");
        String carGear = request.getParameter("gearBox");
        String carFuel = request.getParameter("fuel");
        String carPrice = request.getParameter("price");
        Part part = request.getPart("photo");
        AddNewCarCommand.createNewBrand(carBrand, request);

        String path = request.getSession().getServletContext().getRealPath("/uploads/");

        if(part.getSize()>0) {
            fileName = AddNewCarCommand.extractFileName(part);
            fileName = "picture" + carId + fileName.substring(fileName.indexOf('.'));
            String filePath = path + fileName;
            File fileSaveDir = new File(path);
            if (!fileSaveDir.exists()) {
               fileSaveDir.mkdir();
            }
            part.write(filePath);
        }

        if(car!=null) {
            car.setBrand(carBrand);
            car.setModel(carModel);
            car.setCarcase(carcase);
            car.setColor(carColor);
            car.setCapacity(Double.parseDouble(carCapacity));
            car.setGearBox(carGear);
            car.setFuel(carFuel);
            car.setPrice(Double.parseDouble(carPrice));
            if(fileName.length()!=0) {
                car.setUrl("/uploads/" + fileName);
            }
            try {
                carDAO.update(car);
                forward = Path.PAGE_MAIN;
            } catch (SQLException ex) {
                errorMessage = "Error! Car didn't update";
                request.setAttribute("errorMessage", errorMessage);
                ex.printStackTrace();
                return forward;
            }
        }
        response.sendRedirect(Path.COMMAND_PAGE_MAIN);
        return null;
    }
}