package com.my.project.rent.web.command.admin;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class ControlUsersCommand extends Command {
    private static final Logger logger = LogManager.getLogger(ControlUsersCommand.class);
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        logger.debug("Control users page");
        HttpSession session = request.getSession();
        String forward = Path.PAGE_ERROR_PAGE;
        String errorMessage;

        User user;
        Users userDAO =  DAOFactory.getDAOFactory().getUserDAO();
        int userId = Integer.parseInt(request.getParameter("user_number"));
        int buttonId = Integer.parseInt(request.getParameter("button_id"));
        try {
            user = userDAO.getUserById(userId);
        } catch (SQLException ex) {
            errorMessage = "Failed to find user";
            logger.error(errorMessage, ex);
            request.setAttribute("errorMessage",errorMessage);
            return forward;
        }
        if (user!= null) {
            if(buttonId==0) {
                user.setStatusId(0);
            }
            if(buttonId==1){
                user.setStatusId(1);
            }
            try {
                userDAO.updateStatuses(user);
            } catch (SQLException ex) {
                errorMessage = "Error! User wasn't deleted";
                logger.error(errorMessage, ex);
                request.setAttribute("errorMessage", errorMessage);
                return forward;
            }
        }

        List<User> users;
        try {
            users =userDAO.getAllUsers();
        } catch (SQLException ex) {
            errorMessage = "Error! Failed to get all users";
            logger.error(errorMessage, ex);
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        response.sendRedirect(Path.COMMAND_PAGE_CONTROL_USERS);
        session.setAttribute("users",users);
        return null;
    }
}