package com.my.project.rent.web.command.sort;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ShowCarsByPriceCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String parameter = request.getParameter("sort");
        User user = ((User) session.getAttribute("user"));
        int roleId = -1;
        if(user!=null) {
            roleId = user.getRoleId();
        }
        String forward = Path.PAGE_MAIN;
        List<Car> cars = getCarsByPrice(roleId);
        if("down".equals(parameter)){
            cars.sort(Comparator.comparing(Car::getPrice).reversed());
        }
        session.setAttribute("cars",cars);
        return forward;
    }
   public static List<Car> getCarsByPrice(int roleId){
        List<Car> cars = new ArrayList<>();
        Cars carDAO = DAOFactory.getDAOFactory().getCarDAO();
        try {
            cars = carDAO.getCarsByPrice(roleId);
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return cars;
    }
}