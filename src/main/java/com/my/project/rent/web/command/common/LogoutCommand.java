package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutCommand extends Command {

    private static final long serialVersionUID = -2785976616686657267L;


    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        String forward = Path.COMMAND_PAGE_HOME;
        if (session != null) {
            session.invalidate();
        }
        response.sendRedirect(forward);
        return null;
    }

}