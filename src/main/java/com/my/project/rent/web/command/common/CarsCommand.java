package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Brands;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.dao.interfaces.Classes;
import com.my.project.rent.db.entity.objects.Brand;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.db.entity.objects.CarClass;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarsCommand extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.PAGE_MAIN;
        HttpSession session = request.getSession();
        User user = ((User) session.getAttribute("user"));
        int roleId = -1;
        if (user != null) {
            roleId = user.getRoleId();
        }
        List<Car> cars;
        List<Brand> brands = getAllBrand();
        List<CarClass> classes = getAllClasses();
        cars = getAllCars(roleId);
        session.setAttribute("cars", cars);
        session.setAttribute("brands", brands);
        session.setAttribute("classes", classes);
        return forward;
    }

    public static List<Car> getAllCars(int roleId) {
        List<Car> cars = new ArrayList<>();
        Cars carDAO = DAOFactory.getDAOFactory().getCarDAO();
        try {
            cars = carDAO.getAllCars(roleId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return cars;
    }

    public static List<Brand> getAllBrand() {
        List<Brand> brands = new ArrayList<>();
        Brands brandDAO = DAOFactory.getDAOFactory().getBrandDAO();
        try {
            brands = brandDAO.getAllBrands();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return brands;
    }

    public static List<CarClass> getAllClasses() {
        List<CarClass> classes = new ArrayList<>();
        Classes classesDAO = DAOFactory.getDAOFactory().getClassDAO();
        try {
            classes = classesDAO.getAllClasses();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return classes;
    }
}
