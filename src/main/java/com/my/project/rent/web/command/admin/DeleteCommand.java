package com.my.project.rent.web.command.admin;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.web.command.Command;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class DeleteCommand extends Command {
    private static final Logger logger = LogManager.getLogger(DeleteCommand.class);
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        logger.debug("Delete command");
        HttpSession session = request.getSession();
        String forward = Path.PAGE_ERROR_PAGE;
        String errorMessage;

        Car car = null;
        Cars carDAO =  DAOFactory.getDAOFactory().getCarDAO();
        int carId = Integer.parseInt(request.getParameter("car_number"));
        try {
            car = carDAO.getCarById(carId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (car != null) {
            car.setStatusId(2);
            try {
                carDAO.updateCarStatus(car);
                forward = Path.PAGE_MAIN;
            } catch (SQLException ex) {
                errorMessage = "Error! Car wasn't deleted";
                request.setAttribute("errorMessage", errorMessage);
                logger.error(errorMessage, ex);
                return forward;
            }
        }
        response.sendRedirect(Path.COMMAND_PAGE_MAIN);
        return null;
    }
}