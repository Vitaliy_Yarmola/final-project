package com.my.project.rent.web.filter;

import com.my.project.rent.Path;
import com.my.project.rent.db.entity.enums.Role;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;


public class AccessFilter implements Filter {
	

	// commands access	
	private static final Map<Role, List<String>> roleListMap = new HashMap();
	private static List<String> commons = new ArrayList();
	private static List<String> outOfControl = new ArrayList();
	
	public void destroy() {
		// do nothing
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		if (accessAllowed(request)) {
			chain.doFilter(request, response);
		} else {
			String errorMessage = "You do not have permission to access the requested resource";
			
			request.setAttribute("errorMessage", errorMessage);

			request.getRequestDispatcher(Path.PAGE_ERROR_PAGE)
					.forward(request, response);
		}
	}
	
	private boolean accessAllowed(ServletRequest request) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;

		String commandName = request.getParameter("command");
		if (commandName == null || commandName.isEmpty()) {
			return false;
		}
		
		if (outOfControl.contains(commandName)) {
			return true;
		}
		
		HttpSession session = httpRequest.getSession(false);
		if (session == null) {
			return false;
		}
		
		Role userRole = (Role)session.getAttribute("userRole");
		if (userRole == null) {
			return false;
		}
		
		return roleListMap.get(userRole).contains(commandName)
				|| commons.contains(commandName);
	}

	public void init(FilterConfig fConfig) {

		// roles
		roleListMap.put(Role.ADMIN, asList(fConfig.getInitParameter("admin")));
		roleListMap.put(Role.CLIENT, asList(fConfig.getInitParameter("client")));
		roleListMap.put(Role.MANAGER, asList(fConfig.getInitParameter("manager")));

		// commons
		commons = asList(fConfig.getInitParameter("common"));

		// out of control
		outOfControl = asList(fConfig.getInitParameter("out-of-control"));
	}
	

	private List<String> asList(String str) {
		List<String> list = new ArrayList<>();
		StringTokenizer st = new StringTokenizer(str);
		while (st.hasMoreTokens()) list.add(st.nextToken());
		return list;		
	}
	
}