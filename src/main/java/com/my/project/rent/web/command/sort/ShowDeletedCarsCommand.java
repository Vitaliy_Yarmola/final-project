package com.my.project.rent.web.command.sort;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

public class ShowDeletedCarsCommand extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.PAGE_ERROR_PAGE;
        String errorMessage;
        HttpSession session = request.getSession();
        User user = ((User) session.getAttribute("user"));
        if(user==null || user.getRoleId()!=0) {
            return forward;
        }
        Cars carDAO = DAOFactory.getDAOFactory().getCarDAO();
        List<Car> cars;
        try {
            cars = carDAO.getDeletedCars();
            forward = Path.PAGE_MAIN;
        } catch (SQLException ex) {
            errorMessage = "Error! Something wrong";
            request.setAttribute("errorMessage", errorMessage);
            ex.printStackTrace();
            return forward;
        }
        session.setAttribute("cars", cars);
        return forward;
    }
}