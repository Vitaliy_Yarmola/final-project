package com.my.project.rent.web.command.user;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Orders;
import com.my.project.rent.db.entity.objects.Order;
import com.my.project.rent.db.entity.objects.OrderCarBean;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class OrdersUserCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String forward = Path.PAGE_ERROR_PAGE;
        String errorMessage;
        Order order = null;
        Orders orderDAO = DAOFactory.getDAOFactory().getOrderDAO();
        int orderId= Integer.parseInt(request.getParameter("order_id"));
        String buttonId = request.getParameter("btnId");
        try {
            order = orderDAO.getOrderById(orderId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (order!= null) {
            if(buttonId.equals("btn1")) {
                order.setStatusId(2);
            }
            if(buttonId.equals("btn2")){
                order.setStatusId(6);
            }
            try {
                orderDAO.updateStatus(order);
            } catch (SQLException ex) {
                errorMessage = "Error! Something wrong";
                request.setAttribute("errorMessage", errorMessage);
                ex.printStackTrace();
                return forward;
            }
        }
        response.sendRedirect(Path.COMMAND_PAGE_ORDERS);
        return null;
    }
}