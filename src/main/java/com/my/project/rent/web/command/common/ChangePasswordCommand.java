package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class ChangePasswordCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String forward = Path.PAGE_ERROR_PAGE;
        HttpSession session = request.getSession();
        String errorMessage;
        Users userDAO = DAOFactory.getDAOFactory().getUserDAO();
        User user = ((User) session.getAttribute("user"));
        String password = request.getParameter("current-pass");
        String newPassword= request.getParameter("new-pass");
        String rptPassword = request.getParameter("rpt-pass");

        if (!user.getPassword().equals(password)){
            errorMessage = "Your current password is incorrect";
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        if(newPassword!=null && newPassword.equals(rptPassword)) {
            user.setPassword(newPassword);
        } else {
            errorMessage = "New password doesn't equals repeat password";
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        try {
            userDAO.update(user);
            session.setAttribute("user", user);
            response.sendRedirect(Path.COMMAND_PAGE_PROFILE);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}