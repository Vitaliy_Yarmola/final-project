package com.my.project.rent.web;

import com.my.project.rent.db.entity.enums.Language;
import com.my.project.rent.web.command.Command;
import com.my.project.rent.web.command.CommandContainer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ServletController")
@MultipartConfig()
public class ServletController extends HttpServlet {

    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws IOException, ServletException {
        handler(httpServletRequest,httpServletResponse);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handler(req,resp);
    }

    private void handler(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {

        String forward = null;
        String commandName = request.getParameter("command");

        Command command = CommandContainer.get(commandName);

        if(command !=null) {
            forward = command.execute(request, response);
        }

        if(forward != null){
            RequestDispatcher dispatcher = request.getRequestDispatcher(forward);
            dispatcher.forward(request,response);
        }
    }

    @Override
    public void init() throws ServletException {
        getServletContext().setAttribute("languages", Language.values());
    }
}