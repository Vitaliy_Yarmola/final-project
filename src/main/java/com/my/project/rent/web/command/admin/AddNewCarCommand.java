package com.my.project.rent.web.command.admin;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Brands;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.entity.objects.Brand;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.web.command.Command;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class AddNewCarCommand extends Command {
    private static final Logger logger = LogManager.getLogger(AddNewCarCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String forward = Path.PAGE_ERROR_PAGE;
        String errorMessage;
        Car car = new Car();
        Cars carDAO = DAOFactory.getDAOFactory().getCarDAO();
        car.setBrand(request.getParameter("brand"));
        car.setModel(request.getParameter("model"));
        car.setCarcase(request.getParameter("carcase"));
        car.setColor(request.getParameter("color"));
        car.setCapacity(Double.parseDouble(request.getParameter("capacity")));
        car.setGearBox(request.getParameter("gearBox"));
        car.setFuel(request.getParameter("fuel"));
        car.setPrice(Double.parseDouble(request.getParameter("price")));
        Part part = request.getPart("photo");

        String path = request.getSession().getServletContext().getRealPath("/uploads/");
        createNewBrand(car.getBrand(), request);

        if (part.getSize() > 0) {
            String fileName = extractFileName(part);
            int carId = carDAO.getCount();
            fileName = "picture" + (carId + 1) + fileName.substring(fileName.indexOf('.'));
            String filePath = path + fileName;
            File fileSaveDir = new File(path);
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdir();
            }
            part.write(filePath);
            car.setUrl("/uploads/" + fileName);
        } else {
            car.setUrl("/style/img/noImage.png");
        }
        try {
            carDAO.insert(car);
            forward = Path.PAGE_MAIN;
        } catch (SQLException ex) {
            errorMessage = "Error! Car didn't add to database";
            logger.error(errorMessage, ex);
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        response.sendRedirect(Path.COMMAND_PAGE_MAIN);
        return null;
    }

    protected static String extractFileName(Part part) {
        String header = part.getHeader("content-disposition");
        String[] items = header.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }
        }
        return "";
    }

    public static void createNewBrand(String value, HttpServletRequest request){
        HttpSession session = request.getSession();
        List<Brand> brands = (List<Brand>) session.getAttribute("brands");
        boolean isExisted =false;
        for(Brand brand: brands){
            if (brand.getName().equals(value)) {
                isExisted = true;
                break;
            }
        }
        if(!isExisted){
            logger.debug("Adding new brand to DB");
            Brands brandDAO = DAOFactory.getDAOFactory().getBrandDAO();
            Brand brand = new Brand();
            brand.setName(value);
            try {
                brandDAO.insert(brand);
                logger.trace("Brand name" + brand.getName());
            } catch (SQLException ex) {
                logger.error("Failed to create new brand", ex);
                ex.printStackTrace();
            }
        }
    }

}