package com.my.project.rent.web.command.admin;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;
import com.my.project.rent.web.command.common.RegisterCommand;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;


public class AddManagerCommand extends Command {

    private static final Logger logger = LogManager.getLogger(AddManagerCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String forward = Path.PAGE_ERROR_PAGE;
        String errorMessage;
        User user = new User();
        Users userDAO = DAOFactory.getDAOFactory().getUserDAO();
        String email= request.getParameter("email");
        try {
            if (RegisterCommand.validateEmail(email) && userDAO.isUserEmailExist(email) ){
                errorMessage = "This email exist";
                request.setAttribute("errorMessage", errorMessage);
                return forward;
            }
            user.setEmail(email);
            user.setName(request.getParameter("m_name"));
            user.setSurname(request.getParameter("m_surname"));
            user.setPhone(request.getParameter("m_phone"));
            user.setPassword(request.getParameter("m_pass"));
            user.setRoleId(1);
            user.setLanguageId(0);
            user.setStatusId(0);
            user.setDebtor(false);
            userDAO.insert(user);
        } catch (SQLException ex) {
            errorMessage = "Failed to add new manager";
            logger.error(errorMessage, ex);
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }
        response.sendRedirect(Path.COMMAND_PAGE_USERS);
        return null;
    }
}