package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class EditProfileCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String forward = Path.PAGE_ERROR_PAGE;
        HttpSession session = request.getSession();
        String errorMessage;
        Users userDAO = DAOFactory.getDAOFactory().getUserDAO();
        User user = ((User) session.getAttribute("user"));

        String email = request.getParameter("ps-email");
        String name = request.getParameter("ps-name");
        String surname = request.getParameter("ps-surname");
        String idCode = request.getParameter("id-code");
        String phone = request.getParameter("phone");
        String date = request.getParameter("ps-date");

        if (!validateEmail(email)){
            errorMessage = "Your email is incorrect";
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        user.setName(name);
        user.setSurname(surname);
        if(phone!=null) {
            user.setPhone(phone);
        }
        if(idCode!=null) {
            user.setIdCode(Integer.parseInt(idCode));
        }
        if(date.length()!=0){
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
            try {
                user.setDateBirth(format.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        try {
            if(!user.getEmail().equals(email)) {
                if (userDAO.isUserEmailExist(email)) {
                    errorMessage = "This email is unavailable";
                    forward = Path.PAGE_ERROR_PAGE;
                    request.setAttribute("errorMessage", errorMessage);
                    return forward;
                }
                else user.setEmail(email);
            }
            userDAO.update(user);
            session.setAttribute("user", user);
            response.sendRedirect(Path.COMMAND_PAGE_PROFILE);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static boolean validateEmail(String email){
        return email.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
    }

}