package com.my.project.rent.web.command.admin;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersCommand extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String forward = Path.PAGE_USER_CONTROL;
        HttpSession session = request.getSession();

        List<User> users = getAllCars(request);
        session.setAttribute("users", users);
        return forward;
    }
    protected static List<User> getAllCars(HttpServletRequest request){
        List<User> users = new ArrayList<>();
        Users userDAO = DAOFactory.getDAOFactory().getUserDAO();
        try {
            users = userDAO.getAllUsers();
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return users;
    }
}