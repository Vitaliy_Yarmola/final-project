package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.enums.Language;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.sql.SQLException;

public class ChangeLanguageCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String forward = Path.COMMAND_PAGE_MAIN;
        int languageId = Integer.parseInt(request.getParameter("lang"));

        String languageCode = Language.getLanguage(languageId).getCode();
        Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", languageCode);

        Users userDAO = DAOFactory.getDAOFactory().getUserDAO();
        User user = (User) session.getAttribute("user");
        if(user!=null) {
            user.setLanguageId(languageId);
            try {
                userDAO.updateStatuses(user);
            } catch (SQLException ex) {
                ex.printStackTrace();
                forward = Path.PAGE_ERROR_PAGE;
                request.setAttribute("errorMessage","Failed to set language");
                return forward;
            }
        }

        session.setAttribute("user",user);
        response.sendRedirect(forward);
        return null;
    }
}