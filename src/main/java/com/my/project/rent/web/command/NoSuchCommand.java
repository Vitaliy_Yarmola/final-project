package com.my.project.rent.web.command;

import com.my.project.rent.Path;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NoSuchCommand extends Command {

	private static final long serialVersionUID = -2785976616686657267L;


	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {

		String errorMessage = "No such command";
		request.setAttribute("errorMessage", errorMessage);


		return Path.PAGE_ERROR_PAGE;
	}

}