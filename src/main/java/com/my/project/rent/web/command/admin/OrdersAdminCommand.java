package com.my.project.rent.web.command.admin;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.dao.interfaces.Orders;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.db.entity.objects.Order;
import com.my.project.rent.db.entity.objects.OrderCarBean;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class OrdersAdminCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        String forward = Path.PAGE_ERROR_PAGE;
        String errorMessage;
        Order order = null;
        Orders orderDAO = DAOFactory.getDAOFactory().getOrderDAO();
        Users userDAO =  DAOFactory.getDAOFactory().getUserDAO();
        Cars carDAO =  DAOFactory.getDAOFactory().getCarDAO();
        int orderId= Integer.parseInt(request.getParameter("order_id"));
        String buttonId = request.getParameter("btnId");
        String message = request.getParameter("message");
        String repair = request.getParameter("damage_pay");
        User user = null;
        Car car = null;
        try {
            order = orderDAO.getOrderById(orderId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (order!= null) {
            try {
                if(buttonId.equals("btn1")) {
                    order.setStatusId(0);
                    car = carDAO.getCarById(order.getCarId());
                    car.setStatusId(1);
                    carDAO.updateCarStatus(car);
                    orderDAO.updateStatus(order);
                }
                if(buttonId.equals("btn2")){
                    order.setStatusId(3);
                    order.setMessage(message);
                    orderDAO.updateMessage(order);
                }
                if(buttonId.equals("btn3")){
                    car = carDAO.getCarById(order.getCarId());
                    car.setStatusId(0);
                    carDAO.updateCarStatus(car);
                    order.setStatusId(7);
                    orderDAO.updateStatus(order);
                }
                if(buttonId.equals("btn4")){
                    car = carDAO.getCarById(order.getCarId());
                    car.setStatusId(0);
                    carDAO.updateCarStatus(car);

                    order.setMessage(message);
                    order.setRepair(Double.parseDouble(repair));
                    order.setStatusId(5);
                    orderDAO.updateRepair(order);

                    user =userDAO.getUserById(order.getUserId());
                    user.setDebtor(true);
                    userDAO.updateStatuses(user);
                }
                if (buttonId.equals("btn5")){
                    order.setStatusId(7);
                    user =userDAO.getUserById(order.getUserId());
                    user.setDebtor(false);
                    userDAO.updateStatuses(user);
                    orderDAO.updateStatus(order);
                }
            } catch (SQLException ex) {
                errorMessage = "Error! Something wrong";
                request.setAttribute("errorMessage", errorMessage);
                ex.printStackTrace();
                return forward;
            }
        }
        response.sendRedirect(Path.COMMAND_PAGE_ORDERS);
        return null;
    }
}