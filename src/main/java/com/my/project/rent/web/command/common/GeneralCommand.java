package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GeneralCommand extends Command {
    private static final long serialVersionUID = -2785976616686657267L;
    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) {

        return Path.PAGE_GENERAL;
    }

}