package com.my.project.rent.web;

import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class LanguageListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        i18N(event.getServletContext());
        initLog4J(event.getServletContext());
    }

    private void i18N(ServletContext servletContext) {

        String localesValue = servletContext.getInitParameter("locales");
        if (localesValue == null || localesValue.isEmpty()) {
        } else {
            List<String> locales = new ArrayList<String>();
            StringTokenizer st = new StringTokenizer(localesValue);
            while (st.hasMoreTokens()) {
                String localeName = st.nextToken();
                locales.add(localeName);
            }
            servletContext.setAttribute("locales", locales);
        }
    }
    private void initLog4J(ServletContext servletContext) {
        log("Log4J initialization started");
        try {
            PropertyConfigurator.configure(servletContext.getRealPath("WEB-INF/log4j.properties"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        log("Log4J initialization finished");
    }
    private void log(String msg) {
        System.out.println("[ContextListener] " + msg);
    }
}
