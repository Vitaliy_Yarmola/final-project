package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class RegisterCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String forward = Path.PAGE_ERROR_PAGE;
        HttpSession session = request.getSession();
        String errorMessage;
        Users users = DAOFactory.getDAOFactory("mysql").getUserDAO();
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String phone = request.getParameter("phone");
        String password = request.getParameter("password");
        String passwordConf = request.getParameter("rpt-pass");


        if (!validateEmail(email)){
            errorMessage = "Your email is incorrect";
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        }

        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPhone(phone);
        user.setPassword(password);
        user.setRoleId(2);
        user.setLanguageId(0);
        user.setStatusId(0);
        user.setDebtor(false);
        try {
            if(users.isUserEmailExist(email) || (!password.equals(passwordConf))){
                errorMessage = "This email is unavailable";
                forward  = Path.PAGE_ERROR_PAGE;
                request.setAttribute("errorMessage", errorMessage);
                return forward;
            }
            users.insert(user);
            session.setAttribute("user", user);
            response.sendRedirect(Path.COMMAND_PAGE_LOGIN);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static boolean validateEmail(String email){
        return email.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
    }

}
