package com.my.project.rent.web.command.user;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Orders;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.Car;
import com.my.project.rent.db.entity.objects.Order;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;
import com.my.project.rent.web.command.common.CarsCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CreateOrderCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forward = Path.COMMAND_PAGE_ORDERS;
        HttpSession session = request.getSession();
        User user = ((User) session.getAttribute("user"));
        Users users = DAOFactory.getDAOFactory().getUserDAO();
        Orders orderDAO = DAOFactory.getDAOFactory().getOrderDAO();

        String name = request.getParameter("ps-name");
        String surname = request.getParameter("ps-surname");
        String phone = request.getParameter("phone");
        String id = request.getParameter("id-code");
        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");
        String driver = request.getParameter("driverNeed");
        String carNum = request.getParameter("carId");
        int carId = Integer.parseInt(carNum);
        String date = request.getParameter("ps-date");
        boolean saveData = request.getParameter("dataSave") != null;

        Car car = null;
        try {
            car = DAOFactory.getDAOFactory().getCarDAO().getCarById(carId);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        date1 = date1.substring(0, 10) + ' ' + date1.substring(11);
        date2 = date2.substring(0, 10) + ' ' + date2.substring(11);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Order order = new Order();
        order.setStatusId(1);
        order.setPassportName(name);
        order.setPassportSurname(surname);
        order.setPhone(phone);
        order.setIdCode(Integer.parseInt(id));
        order.setUserId(user.getId());
        order.setCarId(carId);
        try {
            order.setBirth(format.parse(date));
            order.setBeginRent(formatter.parse(date1));
            order.setEndRent(formatter.parse(date2));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (driver != null) {
            order.setDriver(driver.equals("need"));
        } else order.setDriver(false);
        order.setCarId(carId);
        if (car != null) {
            order.setCost(cost(order.getBeginRent(), order.getEndRent(), car.getPrice(), order.isDriver()));
        }
        try {
            orderDAO.insert(order);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (saveData) {
            user.setName(order.getPassportName());
            user.setSurname(order.getPassportSurname());
            user.setPhone(phone);
            user.setIdCode(order.getIdCode());
            user.setDateBirth(order.getBirth());
            try {
                users.update(user);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        List<Car> cars;
        int value = user.getRoleId();
        cars = CarsCommand.getAllCars(value);
        session.setAttribute("cars", cars);
        return forward;
    }

    public Double cost(Date one, Date two, Double price, boolean driver) {
        if(driver){
            price*=1.4;
        }
        long diff = two.getTime() - one.getTime();
        long hours = diff / (1000 * 60 * 60);
        double result = price / 24;
        int finalPrice = (int) (hours * result*100);
        return ((double) finalPrice)/100;
    }
}