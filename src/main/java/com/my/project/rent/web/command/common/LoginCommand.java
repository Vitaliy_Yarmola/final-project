package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.enums.Language;
import com.my.project.rent.db.entity.enums.Role;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.sql.SQLException;

public class LoginCommand extends Command {


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Users users = DAOFactory.getDAOFactory().getUserDAO();

        HttpSession session = request.getSession();

        // obtain email and password from the request
        String email = request.getParameter("email");

        String password = request.getParameter("password");

        // error handler
        String errorMessage;
        String forward = Path.PAGE_ERROR_PAGE;

        if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
            forward = Path.PAGE_LOGIN;
            return forward;
        }

        User user = null;
        try {
            user = users.findUserByEmail(email);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (user == null || !password.equals(user.getPassword())) {
            errorMessage = "Something wrong! Check your email or password!";
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        } else if(user.getStatusId()==1){
            errorMessage = "Your account is blocked!";
            request.setAttribute("errorMessage", errorMessage);
            return forward;
        } else  {
            session.setAttribute("user", user);
            Role userRole = Role.getRole(user);
            session.setAttribute("userRole", userRole);
            Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", Language.getLanguage(user.getLanguageId()).getCode());
            response.sendRedirect(Path.COMMAND_PAGE_MAIN);
        }
        return null;
    }
}