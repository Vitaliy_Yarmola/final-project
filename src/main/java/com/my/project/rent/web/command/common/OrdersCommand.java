package com.my.project.rent.web.command.common;

import com.my.project.rent.Path;
import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.Orders;
import com.my.project.rent.db.entity.objects.OrderCarBean;
import com.my.project.rent.db.entity.enums.Role;
import com.my.project.rent.db.entity.objects.User;
import com.my.project.rent.web.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrdersCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        Orders orderDAO = DAOFactory.getDAOFactory().getOrderDAO();

        HttpSession session = request.getSession();
        String forward = Path.PAGE_ERROR_PAGE;
        User user =  ((User) session.getAttribute("user"));

        Role userRole = null;
        if (user != null) {
            userRole = Role.getRole(user);
        }
        if (userRole == Role.ADMIN || userRole == Role.MANAGER) {
            List<OrderCarBean> ordersAll = new ArrayList<>();
            try {
                ordersAll = orderDAO.getAllOrders();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            forward = Path.PAGE_ADMIN_ORDERS;
            session.setAttribute("orders", ordersAll);
        }

        if (userRole == Role.CLIENT) {
            List<OrderCarBean> orders = new ArrayList<>();
            try {
                orders = orderDAO.getOrdersByUserId(user.getId());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            forward = Path.PAGE_CLIENT_ORDERS;
            session.setAttribute("orders", orders);
        }
        return forward;
    }

}

