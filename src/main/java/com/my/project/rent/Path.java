package com.my.project.rent;

public final class Path {

	// pages
	public static final String PAGE_LOGIN = "/WEB-INF/jsp/login.jsp";
	public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
	public static final String PAGE_PROFILE = "/WEB-INF/jsp/profile.jsp";
	public static final String PAGE_GENERAL= "/WEB-INF/jsp/general.jsp";
	public static final String PAGE_MAIN = "/WEB-INF/jsp/main.jsp";
	public static final String PAGE_ADMIN_ORDERS = "/WEB-INF/jsp/orders_admin.jsp";
	public static final String PAGE_CLIENT_ORDERS = "/WEB-INF/jsp/orders_client.jsp";
	public static final String PAGE_USER_CONTROL = "/WEB-INF/jsp/users_control.jsp";

	//commands
	public static final String COMMAND_PAGE_MAIN="/controller?command=main";
	public static final String COMMAND_PAGE_USERS="/controller?command=users";
	public static final String COMMAND_PAGE_CONTROL_USERS="/controller?command=users";
	public static final String COMMAND_PAGE_ORDERS="/controller?command=orders";
	public static final String COMMAND_PAGE_HOME="/controller?command=home";
	public static final String COMMAND_PAGE_LOGIN = "/controller?command=login";
	public static final String COMMAND_PAGE_PROFILE = "/controller?command=profile";
}