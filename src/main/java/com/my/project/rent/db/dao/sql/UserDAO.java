package com.my.project.rent.db.dao.sql;

import com.my.project.rent.db.dao.DBManager;
import com.my.project.rent.db.dao.Fields;
import com.my.project.rent.db.dao.interfaces.EntityMapper;
import com.my.project.rent.db.dao.interfaces.Users;
import com.my.project.rent.db.entity.objects.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements Users {

    private static UserDAO instance;

    public static synchronized UserDAO getInstance() {
        if (instance == null) {
            instance = new UserDAO();
        }
        return instance;
    }

    private UserDAO() {
    }

    public List<User> getAllUsers() throws SQLException {
        String query = "SELECT * FROM users;";
        Connection con = null;
        Statement stm;
        ResultSet rs;
        ArrayList<User> users = new ArrayList<>();
        try {
            con = DBManager.getConnection();
            stm = con.createStatement();
            stm.executeQuery(query);
            rs = stm.getResultSet();
            UsersMapper mapper = new UsersMapper();
            while (rs.next()) {
              users.add(mapper.mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return users;
    }


    public User getUserById(int id) throws SQLException {
        String query = "SELECT * FROM users WHERE id = ?;";
        Connection con = null;
        PreparedStatement ps;
        ResultSet rs;
        User user = null;

        try {
            con = DBManager.getConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            UsersMapper mapper = new UsersMapper();
            rs.next();
            user = mapper.mapRow(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return user;
    }

    @Override
    public void insert(User user) throws SQLException {
        String query = "INSERT INTO users (email, password, name, surname, phone, role_id, status_id, language_id, debtor) values (?,?,?,?,?,?,?,?,?);";

        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getName());
            statement.setString(4, user.getSurname());
            statement.setString(5, user.getPhone());
            statement.setInt(6, user.getRoleId());
            statement.setInt(7, user.getStatusId());
            statement.setInt(8, user.getLanguageId());
            statement.setInt(9, user.isDebtor()?1:0);

            if (statement.executeUpdate() > 0) {
                rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(User user) throws SQLException {
        String query = "update users set email=?, name=?, surname=?, password=?,  phone=?, role_id=?, ID_code=?, date=?, language_id=?, status_id=?, debtor=? where (id=?);";
        PreparedStatement statement;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getName());
            statement.setString(3, user.getSurname());
            statement.setString(4, user.getPassword());
            statement.setString(5, user.getPhone());
            statement.setInt(6, user.getRoleId());
            statement.setInt(7, user.getIdCode());
            if(user.getDateBirth()!=null) {
                statement.setDate(8, new Date(user.getDateBirth().getTime()));
            } else statement.setDate(8, null);
            statement.setInt(9, user.getLanguageId());
            statement.setInt(10, user.getStatusId());
            statement.setInt(11, user.isDebtor()?1:0);
            statement.setInt(12, user.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateStatuses(User user){
        String query = "update users set role_id=?, language_id=?, status_id=?, debtor=? where (id=?);";
        PreparedStatement statement;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setInt(1, user.getRoleId());
            statement.setInt(2, user.getLanguageId());
            statement.setInt(3, user.getStatusId());
            statement.setInt(4, user.isDebtor()?1:0);
            statement.setInt(5, user.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isUserEmailExist(String email) {
        String query = "SELECT * FROM users WHERE email =?;";

        PreparedStatement statement;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);

            statement.setString(1, email);

            if (statement.executeQuery().next()) {
                return true;
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return false;
    }

    @Override
    public User findUserByEmail(String email) throws SQLException {
        String query = "SELECT * FROM users WHERE email=?;";
        User user = null;
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setString(1, email);
            rs = statement.executeQuery();
            if(rs.next()) {
                UsersMapper mapper = new UsersMapper();
                user = mapper.mapRow(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return user;
    }

    private static class UsersMapper implements EntityMapper<User> {
        @Override
        public User mapRow(ResultSet rs) {
            try {
                User user = new User();
                user.setId(rs.getInt(Fields.ID));
                user.setEmail(rs.getString(Fields.USER_EMAIL));
                user.setPassword(rs.getString(Fields.USER_PASSWORD));
                user.setName(rs.getString(Fields.USER_NAME));
                user.setSurname(rs.getString(Fields.USER_SURNAME));
                user.setRoleId(rs.getInt(Fields.USER_ROLE_ID));
                user.setDebtor(rs.getInt(Fields.USER_DEBTOR)==1);
                user.setPhone(rs.getString(Fields.USER_PHONE_NUMBER));
                user.setIdCode(rs.getInt(Fields.USER_ID_CODE));
                user.setDateBirth(rs.getDate(Fields.USER_DATE));
                user.setLanguageId(rs.getInt(Fields.USER_LANGUAGE_ID));
                user.setStatusId(rs.getInt(Fields.USER_STATUS));
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
