package com.my.project.rent.db.dao.interfaces;

import com.my.project.rent.db.entity.objects.CarClass;

import java.sql.SQLException;
import java.util.List;

public interface Classes {
    List<CarClass> getAllClasses() throws SQLException;
}
