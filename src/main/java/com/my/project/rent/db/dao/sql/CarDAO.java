package com.my.project.rent.db.dao.sql;

import com.my.project.rent.db.dao.DBManager;
import com.my.project.rent.db.dao.Fields;
import com.my.project.rent.db.dao.interfaces.Cars;
import com.my.project.rent.db.dao.interfaces.EntityMapper;
import com.my.project.rent.db.entity.objects.Car;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarDAO implements Cars {
    private static CarDAO instance;

    public static synchronized CarDAO getInstance() {
        if (instance == null) {
            instance = new CarDAO();
        }
        return instance;
    }

    private CarDAO() {
    }

    @Override
    public void insert(Car car) throws SQLException {
        String query = "INSERT INTO cars(picture_url, brand, model, carcase, price, status_id, color, engine_capacity, gear_box, fuel) values(?,?,?,?,?,?,?,?,?,?);";
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, car.getUrl());
            statement.setString(2, car.getBrand());
            statement.setString(3, car.getModel());
            statement.setString(4, car.getCarcase());
            statement.setDouble(5, car.getPrice());
            statement.setInt(6, car.getStatusId());
            statement.setString(7, car.getColor());
            statement.setDouble(8, car.getCapacity());
            statement.setString(9, car.getGearBox());
            statement.setString(10, car.getFuel());
            if (statement.executeUpdate() > 0) {
                rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    car.setId(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        String query = "SELECT COUNT(1) FROM cars;";
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            rs = statement.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    @Override
    public void update(Car car) throws SQLException {
        String query = "UPDATE cars SET picture_url = ?, brand=?, model=?, carcase=?, price=?, status_id=?, color=?, engine_capacity=?, gear_box=?, fuel=? WHERE (id = ?);";

        PreparedStatement statement;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setString(1, car.getUrl());
            statement.setString(2, car.getBrand());
            statement.setString(3, car.getModel());
            statement.setString(4, car.getCarcase());
            statement.setDouble(5, car.getPrice());
            statement.setInt(6, car.getStatusId());
            statement.setString(7, car.getColor());
            statement.setDouble(8, car.getCapacity());
            statement.setString(9, car.getGearBox());
            statement.setString(10, car.getFuel());
            statement.setInt(11, car.getId());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateCarStatus(Car car) {
        String query = "UPDATE cars SET status_id=? WHERE (id = ?);";
        PreparedStatement statement;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setInt(1, car.getStatusId());
            statement.setInt(2, car.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Car> getAllCars(int status) throws SQLException {
        String query = "SELECT * FROM cars where status_id=0 ORDER BY brand, model";
        if (status == 0 || status == 1) {
            query = "SELECT * FROM cars where status_id!=2 ORDER BY brand, model";
        }
        Statement statement;
        ResultSet rs;
        List<Car> eventList = new ArrayList<>();

        try (Connection con = DBManager.getConnection()) {
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            CarMapper mapper = new CarMapper();

            while (rs.next()) {
                eventList.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    @Override
    public List<Car> getDeletedCars() throws SQLException {
        String query = "SELECT * FROM cars where status_id=2 ORDER BY brand, model";
        Statement statement;
        ResultSet rs;
        List<Car> eventList = new ArrayList<>();

        try (Connection con = DBManager.getConnection()) {
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            CarMapper mapper = new CarMapper();

            while (rs.next()) {
                eventList.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    @Override
    public List<Car> getCarsByPrice(int status) throws SQLException {
        String query = "SELECT * FROM cars where status_id=0 ORDER BY price";
        if (status == 0 || status == 1) {
            query = "SELECT * FROM cars where status_id!=2 ORDER BY price";
        }
        Statement statement;
        ResultSet rs;
        List<Car> eventList = new ArrayList<>();
        try (Connection con = DBManager.getConnection()) {
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            CarMapper mapper = new CarMapper();
            while (rs.next()) {
                eventList.add(mapper.mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    @Override
    public List<Car> searchCars(int status, String value) throws SQLException {
        String query = "select * from cars where status_id=0 and brand like ? or model like ? or carcase like ? or color like ? or engine_capacity like ? or fuel like ? or gear_box like ? or price like ? order by brand, model;";
        if (status == 0 || status == 1) {
            query = query.substring(0, 34) + "!=2" + query.substring(36);
        }
        PreparedStatement statement;
        ResultSet rs;
        List<Car> eventList = new ArrayList<>();
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setString(1, "%"+value+"%");
            statement.setString(2, "%"+value+"%");
            statement.setString(3,"%"+value+"%");
            statement.setString(4, "%"+value+"%");
            statement.setString(5, "%"+value+"%");
            statement.setString(6, "%"+value+"%");
            statement.setString(7,"%"+value+"%");
            statement.setString(8, "%"+value+"%");
            rs = statement.executeQuery();
            CarMapper mapper = new CarMapper();
            while (rs.next()) {
                eventList.add(mapper.mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    @Override
    public List<Car> searchSecond(int status, String value) {
        String query = "select * from cars where status_id=0 and concat(lower(cars.brand), ' ', lower(cars.model)) like ? order by brand, model;";
        if (status == 0 || status == 1) {
            query = query.substring(0, 34) + "!=2" + query.substring(36);
        }
        PreparedStatement statement;
        ResultSet rs;
        List<Car> eventList = new ArrayList<>();
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setString(1, "%"+value.toLowerCase()+"%");
            rs = statement.executeQuery();
            CarMapper mapper = new CarMapper();
            while (rs.next()) {
                eventList.add(mapper.mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    @Override
    public List<Car> searchCarsByBrandOrClass(int status, String value) throws SQLException {
        String query = "select * from cars where status_id=0 and brand=? or carcase=? order by brand, model;";
        if (status == 0 || status == 1) {
            query = query.substring(0, 34) + "!=2" + query.substring(36);
        }
        PreparedStatement statement;
        ResultSet rs;
        List<Car> eventList = new ArrayList<>();
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setString(1, value);
            statement.setString(2, value);
            rs = statement.executeQuery();
            CarMapper mapper = new CarMapper();
            while (rs.next()) {
                eventList.add(mapper.mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventList;
    }

    @Override
    public Car getCarById(int id) {
        String query = "SELECT * FROM cars WHERE id=?;";
        Car car = null;
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setInt(1, id);
            rs = statement.executeQuery();
            CarMapper mapper = new CarMapper();
            rs.next();
            car = mapper.mapRow(rs);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return car;
    }

    private static class CarMapper implements EntityMapper<Car> {

        @Override
        public Car mapRow(ResultSet rs) {
            try {
                Car car = new Car();
                car.setId(rs.getInt(Fields.ID));
                car.setUrl(rs.getString(Fields.CAR_PICTURE));
                car.setModel(rs.getString(Fields.CAR_MODEL));
                car.setBrand(rs.getString(Fields.CAR_BRAND));
                car.setCarcase(rs.getString(Fields.CAR_CARCASE));
                car.setPrice(rs.getDouble(Fields.CAR_PRICE));
                car.setStatusId(rs.getInt(Fields.CAR_STATUS));
                car.setColor(rs.getString(Fields.CAR_COLOR));
                car.setCapacity(rs.getDouble(Fields.CAR_CAPACITY));
                car.setGearBox(rs.getString(Fields.CAR_GEAR_BOX));
                car.setFuel(rs.getString(Fields.CAR_FUEL));
                return car;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
