package com.my.project.rent.db.dao;

import com.my.project.rent.db.dao.interfaces.*;
import com.my.project.rent.db.dao.sql.FactoryDAO;

public abstract class DAOFactory {

    public static final String MYSQL = "MySQL";

    public static DAOFactory getDAOFactory() {
        return FactoryDAO.getInstance();
    }

    public static DAOFactory getDAOFactory(String name) {
        if (MYSQL.equalsIgnoreCase(name)) {
            return FactoryDAO.getInstance();
        }
        throw new RuntimeException("Unknown factory");
    }

    public abstract Users getUserDAO();

    public abstract Orders getOrderDAO();

    public abstract Cars getCarDAO();

    public abstract Classes getClassDAO();

    public abstract Brands getBrandDAO();
}

