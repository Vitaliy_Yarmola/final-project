package com.my.project.rent.db.dao.interfaces;

import com.my.project.rent.db.entity.objects.Order;
import com.my.project.rent.db.entity.objects.OrderCarBean;

import java.sql.SQLException;
import java.util.List;

public interface Orders {
    void insert(Order order) throws SQLException;

    void updateStatus(Order order) throws SQLException;

    void updateRepair(Order order) throws SQLException;

    void updateMessage(Order order) throws SQLException;

    List<OrderCarBean> getAllOrders() throws SQLException;

    Order getOrderById(int id) throws SQLException;
    List<OrderCarBean> getOrdersByUserId(int id) throws SQLException;
}
