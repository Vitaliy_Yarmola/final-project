package com.my.project.rent.db.dao.sql;

import com.my.project.rent.db.dao.DBManager;
import com.my.project.rent.db.dao.Fields;
import com.my.project.rent.db.dao.interfaces.EntityMapper;
import com.my.project.rent.db.dao.interfaces.Orders;
import com.my.project.rent.db.entity.objects.Order;
import com.my.project.rent.db.entity.objects.OrderCarBean;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDAO implements Orders {

    private static OrderDAO instance;

    public synchronized static OrderDAO getInstance() {
        if (instance == null) {
            instance = new OrderDAO();
        }
        return instance;
    }

    @Override
    public void insert(Order order) throws SQLException {
        String query = "INSERT INTO orders(status_id, cost, driver, name, surname, " +
                "ID_code, phone, begin_rent, end_rent, user_id, car_id, date_birth) values(?,?,?,?,?,?,?,?,?,?,?,?);";
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, order.getStatusId());
            statement.setDouble(2, order.getCost());
            statement.setInt(3, order.isDriver() ? 1 : 0);
            statement.setString(4, order.getPassportName());
            statement.setString(5, order.getPassportSurname());
            statement.setInt(6, order.getIdCode());
            statement.setString(7, order.getPhone());
            statement.setTimestamp(8, new Timestamp(order.getBeginRent().getTime()));
            statement.setTimestamp(9, new Timestamp(order.getEndRent().getTime()));
            statement.setInt(10, order.getUserId());
            statement.setInt(11, order.getCarId());
            statement.setDate(12, new Date(order.getBirth().getTime()));
            if (statement.executeUpdate() > 0) {
                rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    order.setId(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateStatus(Order order) throws SQLException {
        String query = "UPDATE orders SET status_id = ? WHERE (id = ?);";
        PreparedStatement statement;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setInt(1, order.getStatusId());
            statement.setInt(2, order.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateRepair(Order order) throws SQLException {
        String query = "UPDATE orders SET status_id = ?, repair=?, message = ? WHERE (id = ?);";
        PreparedStatement statement;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setInt(1, order.getStatusId());
            statement.setDouble(2, order.getRepair());
            statement.setString(3, order.getMessage());
            statement.setInt(4, order.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateMessage(Order order) throws SQLException {
        String query = "UPDATE orders SET status_id = ?,message = ? WHERE (id = ?);";
        PreparedStatement statement;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setInt(1, order.getStatusId());
            statement.setString(2, order.getMessage());
            statement.setInt(3, order.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<OrderCarBean> getAllOrders() {
        String query = "select users.id as user_id, cars.id as car_id, orders.id as order_id, users.name as name, users.surname as surname," +
                " users.status_id as user_status, users.phone as phone, cars.brand as brand, cars.model as model, orders.cost as cost, orders.driver as driver, orders.begin_rent as start_rent," +
                " orders.end_rent as end_rent, orders.status_id as order_status, users.debtor as debtor from cars join orders on orders.car_id = cars.id" +
                " join users on users.id=orders.user_id order by order_id desc;";
        List<OrderCarBean> orders = new ArrayList<>();
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            rs = statement.executeQuery();
            AllOrdersBeanMapper mapper = new AllOrdersBeanMapper();

            while (rs.next()) {
                orders.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public Order getOrderById(int id) throws SQLException {
        String query = "SELECT * FROM orders WHERE id=?;";
        Order order = null;
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            statement.setInt(1, id);
            rs = statement.executeQuery();
            OrdersMapper mapper = new OrdersMapper();
            rs.next();
            order = mapper.mapRow(rs);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return order;
    }

    @Override
    public List<OrderCarBean> getOrdersByUserId(int id) throws SQLException {
        String query = "select orders.id as order_id, cars.brand as brand, cars.model as model, orders.cost as cost, orders.driver as driver, orders.begin_rent as start_rent, " +
                "orders.end_rent as end_rent, orders.status_id as order_status, orders.message as message, orders.repair as repair from cars join orders on orders.car_id = cars.id " +
                "join users on users.id=orders.user_id where users.id = ? order by order_id desc;";
        List<OrderCarBean> selected = new ArrayList<>();
        Connection con = null;
        PreparedStatement statement;
        ResultSet rs;
        try {
            con = DBManager.getConnection();
            statement = con.prepareStatement(query);
            statement.setInt(1, id);
            rs = statement.executeQuery();
            OrdersBeanMapper mapper = new OrdersBeanMapper();

            while (rs.next()) {
                selected.add(mapper.mapRow(rs));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return selected;
    }

    private static class OrdersBeanMapper implements EntityMapper<OrderCarBean> {
        @Override
        public OrderCarBean mapRow(ResultSet rs) {
            try {
                OrderCarBean order = new OrderCarBean();
                order.setOrderId(rs.getInt(Fields.ORDER_ID));
                order.setBrand(rs.getString(Fields.CAR_BRAND));
                order.setModel(rs.getString(Fields.CAR_MODEL));
                order.setCost(rs.getDouble(Fields.ORDER_COST));
                order.setDriver(rs.getInt(Fields.DRIVER_IN_ORDER) == 1);
                order.setStartRent(rs.getTimestamp(Fields.ORDER_CAR_START));
                order.setEndRent(rs.getTimestamp(Fields.ORDER_CAR_END));
                order.setOrderStatusId(rs.getInt(Fields.ORDER_CAR_STATUS));
                order.setMessage(rs.getString(Fields.ORDER_MESSAGE));
                order.setRepair(rs.getDouble(Fields.ORDER_CAR_REPAIR));
                return order;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private static class AllOrdersBeanMapper implements EntityMapper<OrderCarBean> {
        @Override
        public OrderCarBean mapRow(ResultSet rs) {
            try {
                OrderCarBean order = new OrderCarBean();
                order.setUserId(rs.getInt(Fields.USER_ID));
                order.setCarId(rs.getInt(Fields.CAR_ID));
                order.setOrderId(rs.getInt(Fields.ORDER_ID));
                order.setName(rs.getString(Fields.USER_NAME));
                order.setSurname(rs.getString(Fields.USER_SURNAME));
                order.setUserStatus(rs.getInt(Fields.USER_STATUS_ORDER));
                order.setPhone(rs.getString(Fields.USER_PHONE_NUMBER));
                order.setBrand(rs.getString(Fields.CAR_BRAND));
                order.setModel(rs.getString(Fields.CAR_MODEL));
                order.setCost(rs.getDouble(Fields.ORDER_COST));
                order.setDriver(rs.getInt(Fields.DRIVER_IN_ORDER) == 1);
                order.setStartRent(rs.getTimestamp(Fields.ORDER_CAR_START));
                order.setEndRent(rs.getTimestamp(Fields.ORDER_CAR_END));
                order.setOrderStatusId(rs.getInt(Fields.ORDER_CAR_STATUS));
                order.setDebtor(rs.getInt(Fields.USER_DEBTOR)==1);
                return order;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private static class OrdersMapper implements EntityMapper<Order> {
        @Override
        public Order mapRow(ResultSet rs) {
            try {
                Order order = new Order();
                order.setId(rs.getInt(Fields.ID));
                order.setStatusId(rs.getInt(Fields.ORDER_STATUS));
                order.setCost(rs.getDouble(Fields.ORDER_COST));
                order.setDriver(rs.getInt(Fields.DRIVER_IN_ORDER) == 1);
                order.setPassportName(rs.getString(Fields.NAME_FOR_ORDER));
                order.setPassportSurname(rs.getString(Fields.SURNAME_FOR_ORDER));
                order.setIdCode(rs.getInt(Fields.ID_CODE_FOR_ORDER));
                order.setPhone(rs.getString(Fields.USER_PHONE_NUMBER));
                order.setBeginRent(rs.getDate(Fields.ORDER_BEGIN));
                order.setEndRent(rs.getDate(Fields.END_OF_ORDER));
                order.setUserId(rs.getInt(Fields.USER_ID));
                order.setCarId(rs.getInt(Fields.CAR_ID));
                order.setMessage(rs.getString(Fields.ORDER_MESSAGE));
                order.setRepair(rs.getDouble(Fields.ORDER_CAR_REPAIR));
                return order;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
