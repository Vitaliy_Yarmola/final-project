package com.my.project.rent.db.dao.interfaces;

import com.my.project.rent.db.entity.objects.User;

import java.sql.SQLException;
import java.util.List;

public interface Users {
    List<User> getAllUsers() throws SQLException;

    User getUserById(int id) throws SQLException;

    void insert(User user) throws SQLException;

    void update(User user) throws SQLException;

    void updateStatuses(User user) throws SQLException;

    boolean isUserEmailExist(String email) throws SQLException;

    User findUserByEmail(String login) throws SQLException;

}
