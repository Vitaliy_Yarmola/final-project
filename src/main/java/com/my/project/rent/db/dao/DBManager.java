package com.my.project.rent.db.dao;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.TreeMap;

public class DBManager {
    private static final DBManager instance;
    private static final String realPath;

    public static DBManager getInstance() {
        return instance;
    }
    private DBManager() {
    }
    private static final Map<Integer, String> roles = new TreeMap<>();

    static {
        instance = new DBManager();
        realPath = instance.getRealPath();
        String query = "SELECT * FROM roles;";
        try (Connection con = getConnection()) {
            Statement statement = con.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                roles.put(resultSet.getInt(1), resultSet.getString(2));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        MysqlConnectionPoolDataSource ds = new MysqlConnectionPoolDataSource();
        try {
            ds.setUser(getFromProperties("user"));
            ds.setPassword(getFromProperties("password"));
            ds.setServerTimezone(getFromProperties("timezone"));
            ds.setURL(getFromProperties("url"));
        } catch (IOException e) {
            throw new SQLException("Can`t connect to database, missed properties", e);
        }
        return ds.getConnection();
    }

    private static String getFromProperties(String key) throws IOException {
        Properties property = new Properties();
        String path = realPath+"/app.properties";
        try(FileInputStream fis = new FileInputStream(path)) {
            property.load(fis);
            return property.getProperty("connection." + key);
        } catch (IOException e) {
            throw new IOException("Can`t reach app.properties file", e);
        }
    }
    private String getRealPath() {
        String path = Objects.requireNonNull(this.getClass().getClassLoader().getResource("")).getPath();
        String fullPath;
        try {
            fullPath = URLDecoder.decode(path, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            fullPath = path;
        }
        String[] pathArr = fullPath.split("/(?:WEB-INF|target)");
        fullPath = pathArr[0];

        return fullPath;
    }


}
