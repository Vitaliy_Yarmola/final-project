package com.my.project.rent.db.dao.interfaces;

import com.my.project.rent.db.entity.objects.Brand;
import com.my.project.rent.db.entity.objects.CarClass;

import java.sql.SQLException;
import java.util.List;

public interface Brands {
    void insert(Brand brand) throws SQLException;
    List<Brand> getAllBrands() throws SQLException;
}
