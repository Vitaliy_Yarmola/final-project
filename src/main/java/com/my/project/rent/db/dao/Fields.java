package com.my.project.rent.db.dao;

/**
 * Holder for fields names of DB tables and beans.
 *
 */
public final class Fields {
	
	// entities
	public static final String ID = "id";
	//user
	public static final String USER_EMAIL = "email";
	public static final String USER_PASSWORD = "password";
	public static final String USER_NAME = "name";
	public static final String USER_SURNAME = "surname";
	public static final String USER_ROLE_ID = "role_id";
	public static final String USER_STATUS = "status_id";
	public static final String USER_PHONE_NUMBER = "phone";
	public static final String USER_ID_CODE = "ID_code";
	public static final String USER_DATE = "date";
	public static final String USER_LANGUAGE_ID = "language_id";
	public static final String USER_DEBTOR = "debtor";

	//car
	public static final String CAR_BRAND = "brand";
	public static final String CAR_PICTURE = "picture_url";
	public static final String CAR_MODEL= "model";
	public static final String CAR_CARCASE = "carcase";
	public static final String CAR_PRICE = "price";
	public static final String CAR_STATUS = "status_id";
	public static final String CAR_COLOR = "color";
	public static final String CAR_CAPACITY = "engine_capacity";
	public static final String CAR_GEAR_BOX = "gear_box";
	public static final String CAR_FUEL = "fuel";
	// order
	public static final String ORDER_ID = "order_id";
	public static final String ORDER_STATUS = "status_id";
	public static final String ORDER_COST = "cost";
	public static final String DRIVER_IN_ORDER = "driver";
	public static final String NAME_FOR_ORDER = "name";
	public static final String SURNAME_FOR_ORDER = "surname";
	public static final String PASSWORD_NUMBER_FOR_ORDER = "passport_number";
	public static final String ID_CODE_FOR_ORDER = "ID_code";
	public static final String ORDER_BEGIN = "begin_rent";
	public static final String END_OF_ORDER = "end_rent";
	public static final String USER_ID = "user_id";
	public static final String CAR_ID = "car_id";

	//beans
	public static final String ORDER_CAR_STATUS = "order_status";
	public static final String USER_STATUS_ORDER = "user_status";
	public static final String ORDER_CAR_START = "start_rent";
	public static final String ORDER_CAR_END = "end_rent";
	public static final String ORDER_MESSAGE = "message";
	public static final String ORDER_CAR_REPAIR = "repair";
}