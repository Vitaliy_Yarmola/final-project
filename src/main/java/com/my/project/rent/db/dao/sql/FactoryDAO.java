package com.my.project.rent.db.dao.sql;

import com.my.project.rent.db.dao.DAOFactory;
import com.my.project.rent.db.dao.interfaces.*;

public class FactoryDAO extends DAOFactory {

    private static FactoryDAO instance;

    private FactoryDAO() {
    }

    public synchronized static DAOFactory getInstance() {
        if (instance == null) {
            instance = new FactoryDAO();
        }
        return instance;
    }

    @Override
    public Users getUserDAO() {
        return UserDAO.getInstance();
    }

    @Override
    public Orders getOrderDAO() {
        return OrderDAO.getInstance();
    }

    @Override
    public Cars getCarDAO() {
        return CarDAO.getInstance();
    }

    @Override
    public Classes getClassDAO() {
        return ClassDAO.getInstance();
    }

    @Override
    public Brands getBrandDAO() {
        return BrandDAO.getInstance();
    }

}
