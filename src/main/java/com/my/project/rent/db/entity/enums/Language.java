package com.my.project.rent.db.entity.enums;

public enum Language {
    ENGLISH("en"), UKRAINIAN("uk"), RUSSIAN("ru");
    private String code;

    Language(String code) {
        this.code = code;
    }

    public static Language getLanguage(int languageId) {
        return Language.values()[languageId];
    }

    public String getCode() {
        return code;
    }
}
