package com.my.project.rent.db.dao.sql;

import com.my.project.rent.db.dao.DBManager;
import com.my.project.rent.db.dao.Fields;
import com.my.project.rent.db.dao.interfaces.Brands;
import com.my.project.rent.db.dao.interfaces.EntityMapper;
import com.my.project.rent.db.entity.objects.Brand;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BrandDAO implements Brands {

    private static BrandDAO instance;

    public synchronized static BrandDAO getInstance() {
        if (instance == null) {
            instance = new BrandDAO();
        }
        return instance;
    }

    @Override
    public void insert(Brand brand) {
        String query = "INSERT INTO brands(name) values(?);";
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, brand.getName());
            if (statement.executeUpdate() > 0) {
                rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    brand.setId(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Brand> getAllBrands() throws SQLException {
        String query = "SELECT * FROM brands;";
        List<Brand> brands = new ArrayList<>();
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            rs = statement.executeQuery();
            BrandMapper mapper = new BrandMapper();
            while (rs.next()) {
                brands.add(mapper.mapRow(rs));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return brands;
    }

    private static class BrandMapper implements EntityMapper<Brand> {
        @Override
        public Brand mapRow(ResultSet rs) {
            try {
                Brand brand = new Brand();
                brand.setId(rs.getInt(Fields.ID));
                brand.setName(rs.getString(Fields.USER_NAME));
                return brand;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
