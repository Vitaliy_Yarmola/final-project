package com.my.project.rent.db.dao.interfaces;

import com.my.project.rent.db.entity.objects.Car;

import java.sql.SQLException;
import java.util.List;

public interface Cars {
    void insert(Car car) throws SQLException;

    void update(Car car) throws SQLException;

    void updateCarStatus(Car car) throws SQLException;

    int getCount();

    List<Car> getAllCars(int status) throws SQLException;

    List<Car> getDeletedCars() throws SQLException;

    List<Car> getCarsByPrice(int status) throws SQLException;

    List<Car> searchCars(int status, String value) throws SQLException;

    List<Car> searchSecond(int status, String value) throws SQLException;

    List<Car> searchCarsByBrandOrClass(int status, String value) throws SQLException;

    Car getCarById(int id) throws SQLException;
}
