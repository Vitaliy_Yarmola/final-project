package com.my.project.rent.db.dao.sql;

import com.my.project.rent.db.dao.DBManager;
import com.my.project.rent.db.dao.Fields;
import com.my.project.rent.db.dao.interfaces.Classes;
import com.my.project.rent.db.dao.interfaces.EntityMapper;
import com.my.project.rent.db.entity.objects.CarClass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClassDAO implements Classes {
    private static ClassDAO instance;

    public synchronized static ClassDAO getInstance() {
        if (instance == null) {
            instance = new ClassDAO();
        }
        return instance;
    }

    @Override
    public List<CarClass> getAllClasses(){
        String query = "SELECT * FROM classes;";
        List<CarClass> carClasses = new ArrayList<>();
        PreparedStatement statement;
        ResultSet rs;
        try (Connection con = DBManager.getConnection()) {
            statement = con.prepareStatement(query);
            rs = statement.executeQuery();
            ClassesMapper mapper = new ClassesMapper();
            while (rs.next()) {
                carClasses.add(mapper.mapRow(rs));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return carClasses;
    }

    private static class ClassesMapper implements EntityMapper<CarClass> {
        @Override
        public CarClass mapRow(ResultSet rs) {
            try {
                CarClass carClass = new CarClass();
                carClass.setId(rs.getInt(Fields.ID));
                carClass.setName(rs.getString(Fields.USER_NAME));
                return carClass;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
