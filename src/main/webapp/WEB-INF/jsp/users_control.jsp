<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ page pageEncoding="UTF-8" %>
<!doctype html>
<html>
<c:set var="title" value="Users" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="table-back">
    <div class="form-inline float-left " style="padding: 10px">
        <div class="form-group">
            <button type="button" class="btn btn-primary float-left" style=" z-index: 1;" data-toggle="modal"
                    data-target="#addManager">
                <fmt:message key="users.control.button.add.manager"/>
            </button>
        </div>
    </div>

    <table id="usersControlTable" class="table table-striped table-bordered table-sm text-center"
           cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="th-sm"><fmt:message key="users.control.table.column.email"/></th>
            <th class="th-sm"><fmt:message key="users.control.table.column.name"/></th>
            <th class="th-sm"><fmt:message key="users.control.table.column.surname"/></th>
            <th class="th-sm"><fmt:message key="users.control.table.column.role"/></th>
            <th class="th-sm"><fmt:message key="users.control.table.column.status"/></th>
            <th class="th-sm"><fmt:message key="users.control.table.column.debtor"/></th>
            <th class="th-sm"><fmt:message key="users.control.table.column.actions"/></th>
        </tr>

        </thead>
        <tbody>
        <c:forEach var="users" items="${users}">
            <tr>
                <td>${users.email}</td>
                <td>${users.name}</td>
                <td>${users.surname}</td>
                <td>
                    <c:if test="${users.roleId==0}"><fmt:message key="user.role.admin"/></c:if>
                    <c:if test="${users.roleId==1}"><fmt:message key="user.role.manager"/></c:if>
                    <c:if test="${users.roleId==2}"><fmt:message key="user.role.client"/></c:if>
                </td>
                <td>
                    <c:if test="${users.statusId==0}"><fmt:message key="user.status.unblocked"/></c:if>
                    <c:if test="${users.statusId==1}"><fmt:message key="user.status.blocked"/></c:if>
                </td>
                <td><fmt:message key="user.debtor.${users.debtor}"/></td>
                <td>
                    <c:if test="${users.roleId!=0}">
                        <c:if test="${users.statusId==1}">
                            <form action="controller" method="post">
                                <input name="button_id" type="hidden" value="0">
                                <input name="user_number" type="hidden" value="${users.id}">
                                <input type="hidden" name="command" value="change_status"/>
                                <input class="btn btn-primary" type="submit" value="<fmt:message key="user.control.buttons.unblock"/>"/>
                            </form>
                        </c:if>
                        <c:if test="${users.statusId==0}">
                            <form action="controller" method="post">
                                <input name="button_id" type="hidden" value="1">
                                <input name="user_number" type="hidden" value="${users.id}">
                                <input type="hidden" name="command" value="change_status"/>
                                <input class="btn btn-danger" type="submit" value="<fmt:message key="user.control.buttons.block"/>"/>
                            </form>
                        </c:if>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="modal fade" id="addManager" tabindex="-1" role="dialog"
     aria-labelledby="modalManager" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="controller"  id="manager-add" method="post">
                <div class="modal-header text-center">
                    <h5 class="info-color font-weight-bold" id="modalManager"><fmt:message key="user.control.modal.add.manager.title"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="<fmt:message key="main.modal.close"/>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>
                        <label for="email" class="text-dark"><fmt:message key="user.control.modal.add.manager.label.email"/></label>
                        <input name="email" type="text" id="email" class="form-control mb-2"
                               placeholder="<fmt:message key="user.control.modal.add.manager.input.email"/>" onkeyup="checkInputData()">
                    </div>
                    <div>
                        <label for="m_phone" class="text-dark"><fmt:message key="user.control.modal.add.manager.label.phone"/></label>
                        <input name="m_phone" type="text" id="m_phone" class="form-control mb-2"
                               placeholder="<fmt:message key="user.control.modal.add.manager.input.phone"/>" onkeyup="checkInputData()">
                    </div>
                    <div>
                        <label for="m_name" class="text-dark"><fmt:message key="user.control.modal.add.manager.label.name"/></label>
                        <input name="m_name" type="text" id="m_name" class="form-control mb-2"
                               placeholder="<fmt:message key="user.control.modal.add.manager.input.name"/>" onkeyup="checkInputData()">
                    </div>
                    <div>
                        <label for="m_surname" class="text-dark"><fmt:message key="user.control.modal.add.manager.label.surname"/></label>
                        <input name="m_surname" type="text" id="m_surname" class="form-control mb-2"
                               placeholder="<fmt:message key="user.control.modal.add.manager.input.surname"/>" onkeyup="checkInputData()">
                    </div>
                    <div>
                        <label for="m_pass" class="text-dark"><fmt:message key="user.control.modal.add.manager.label.password"/></label>
                        <input name="m_pass" type="password" id="m_pass" class="form-control mb-2"
                               placeholder="<fmt:message key="user.control.modal.add.manager.input.password"/>" onkeyup="checkInputData()">
                    </div>
                    <div>
                        <label for="rpt_pass" class="text-dark"><fmt:message key="user.control.modal.add.manager.label.password.rpt"/></label>
                        <input name="rpt_pass" type="password" id="rpt_pass" class="form-control mb-2"
                               placeholder="<fmt:message key="user.control.modal.add.manager.input.password.rpt"/>" onkeyup="checkInputData()">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                    </button>
                    <input type="hidden" name="command" value="add_manager"/>
                    <input class="btn btn-primary" type="submit" id="add_manager" value="<fmt:message key="user.control.modal.add.manager.submit"/>" disabled/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function checkInputData() {
        const email = document.getElementById("email").value;
        const name = document.getElementById("m_name").value;
        const surname = document.getElementById("m_surname");
        const phone = document.getElementById("m_phone").value;
        const password = document.getElementById("m_pass").value;
        const password2 = document.getElementById("rpt_pass").value;
        const register = document.getElementById("add_manager");

        if (name.length !== 0 && surname.length !== 0
            && phone.length !== 0 && email.length !== 0 && password===password2
            && password.length !== 0 && password2.length !== 0) {
            register.removeAttribute('disabled');
        } else {
            register.setAttribute('disabled', 'disabled');
        }
    }

    $(document).ready(function() {
        $('#usersControlTable').DataTable({
            searching: true, paging: true, info: true, "lengthChange": false,
            "lengthMenu": [[25], [25]],
            "ordering":false,
            "language": {
                <c:if test="${user.languageId==0}">
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "Showing 0 to 0 of 0 entries",
                "search": "Search:",
                "paginate": {
                    "previous": "Previous page",
                    "next": "Next",
                }
                </c:if>
                <c:if test="${user.languageId==1}">
                "emptyTable": "Немає жодних доступних записів",
                "info": "Показано від _START_ до _END_ із _TOTAL_ записів",
                "infoFiltered":   "",
                "infoEmpty": "Немає даних",
                "search": "Шукати:",
                "paginate": {
                    "previous": "Попередня",
                    "next": "Наступна",
                }
                </c:if>
                <c:if test="${user.languageId==2}">
                "emptyTable": "Нет никаких доступных записей",
                "info": "Показано от _START_ до _END_ с _TOTAL_ записей",
                "infoFiltered":   "",
                "infoEmpty": "Нету данных",
                "search": "Искать:",
                "paginate": {
                    "previous": "Предыдущая",
                    "next": "Следующая",
                }
                </c:if>
            }
        });
    } );
</script>
</body>
</html>
