<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<c:set var="title" value="Orders" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="table-back2">

    <table id="orderAdmin" class="table table-striped table-bordered table-sm text-center"
           cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.name"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.surname"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.phone"/></th>
            <th class="th-sm"><fmt:message key="header.language"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.debtor"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.brand"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.model"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.cost"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.driver"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.start.rent"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.end.rent"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.status"/></th>
            <th class="th-sm"><fmt:message key="orders.admin.table.column.actions"/></th>
        </tr>

        </thead>
        <tbody>
        <c:forEach var="orders" items="${orders}">
            <tr>
                <td>${orders.name}</td>
                <td>${orders.surname}</td>
                <td>${orders.phone}</td>
                <td><fmt:message key="orders.admin.lang.key.${user.languageId}"/></td>
                <td><fmt:message key="user.debtor.${orders.debtor}"/></td>
                <td>${orders.brand}</td>
                <td>${orders.model}</td>
                <td>${orders.cost}</td>
                <td><fmt:message key="order.admin.driver.${orders.driver}"/></td>
                <td>${orders.startRent}</td>
                <td>${orders.endRent}</td>
                <td><fmt:message key="order.admin.status.id.${orders.orderStatusId}"/></td>
                <td>
                    <c:if test="${orders.orderStatusId==1}">
                        <form action="controller" method="get">
                            <input type="hidden" name="order_id" value="${orders.orderId}"/>
                            <input type="hidden" name="btnId" value="btn1"/>
                            <input type="hidden" name="command" value="admin_order"/>
                            <div class="btn-group" role="group">
                                <input class="btn btn-primary" type="submit" value="<fmt:message key="orders.admin.table.button.apply"/>"/>
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#refuseModal" onclick="sendParams(${orders.orderId})">
                                    <fmt:message key="orders.admin.table.button.refuse"/>
                                </button>
                            </div>
                        </form>
                    </c:if>
                    <c:if test="${orders.orderStatusId==2}">
                        <form action="controller" method="post">
                            <input type="hidden" name="order_id" value="${orders.orderId}"/>
                            <input type="hidden" name="btnId" value="btn3"/>
                            <input type="hidden" name="command" value="admin_order"/>
                            <div class="btn-group" role="group">
                                <input class="btn btn-primary" type="submit" value="<fmt:message key="orders.admin.table.button.returned"/>"/>
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#damageModal" onclick="sendParams(${orders.orderId})">
                                    <fmt:message key="orders.admin.table.button.breakage"/>
                                </button>
                            </div>
                        </form>
                    </c:if>
                    <c:if test="${orders.orderStatusId==6}">
                        <form class="my-2 my-md-0 mr-md-3" action="controller" method="post">
                            <input type="hidden" name="order_id" value="${orders.orderId}"/>
                            <input type="hidden" name="btnId" value="btn5"/>
                            <input type="hidden" name="command" value="admin_order"/>
                            <input class="btn btn-primary" type="submit" value="<fmt:message key="orders.admin.table.button.success"/>"/>
                        </form>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="modal fade" id="damageModal" tabindex="-1" role="dialog"
     aria-labelledby="modalDamage" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="controller" id="order_car_damage" method="post">
                <div class="modal-header text-center">
                    <h5 class="info-color font-weight-bold" id="modalDamage"><fmt:message key="orders.admin.modal.broken.title"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="<fmt:message key="main.modal.close"/>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="btnId" type="hidden" value="btn4">
                    <input name="order_id" id="order_number" type="hidden" value="0">

                    <div class="form-group">
                        <label for="message2"><fmt:message key="orders.admin.modal.label.broken.message"/></label>
                        <textarea name="message" class="form-control" id="message2" rows="3" onkeyup="checkRepair()"></textarea>
                    </div>
                    <div>
                        <label for="damage_pay" class="text-dark"><fmt:message key="orders.admin.modal.label.broken.damage.pay"/></label>
                        <input name="damage_pay" type="number" id="damage_pay" class="form-control mb-2"
                               placeholder="Enter cost" onkeyup="checkRepair()">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                    </button>
                    <input type="hidden" name="command" value="admin_order"/>
                    <input class="btn btn-primary" id="broken" type="submit" value="<fmt:message key="orders.admin.modal.broken.button.submit"/>"/>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="refuseModal" tabindex="-1" role="dialog"
     aria-labelledby="modalRefuse" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="controller" id="order_refuse" method="post">
                <div class="modal-header text-center">
                    <h5 class="info-color font-weight-bold" id="modalRefuse"><fmt:message key="orders.admin.modal.refuse.title"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="<fmt:message key="main.modal.close"/>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="order_id" id="order_id" type="hidden" value="0">
                    <input name="btnId" type="hidden" value="btn2">
                    <div class="form-group">
                        <label for="message"><fmt:message key="orders.admin.modal.refuse.label.message"/></label>
                        <textarea name="message" class="form-control" id="message" rows="3" onkeyup="checkMessage()"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                    </button>
                    <input type="hidden" name="command" value="admin_order"/>
                    <input class="btn btn-danger" id="ref-btn" type="submit" value="<fmt:message key="orders.admin.table.button.refuse"/>" disabled/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function sendParams(id) {
        $('#order_id').val(id);
        $('#order_number').val(id);
    }

    function checkMessage() {
        const message = $('#message').val();
        if (message.length !== 0) {
            $('#ref-btn').removeAttr('disabled');
        } else {
            $('#ref-btn').attr('disabled', 'disabled');
        }
    }

    function checkRepair() {
        const message = $('#message2').val();
        const cost = $('#damage_pay').val();
        if (message.length !== 0 && cost.length!==0) {
            $('#broken').removeAttr('disabled');
        } else {
            $('#broken').attr('disabled', 'disabled');
        }
    }

    $(document).ready(function() {
        $('#orderAdmin').DataTable({
            searching: true, paging: true, info: true, "lengthChange": false,
            "lengthMenu": [[20], [20]],
            "ordering":false,
            "language": {
                <c:if test="${user.languageId==0}">
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoFiltered":   "(filtered from _MAX_ total entries)",
                "infoEmpty": "Showing 0 to 0 of 0 entries",
                "search": "Search:",
                "paginate": {
                    "previous": "Previous page",
                    "next": "Next",
                }
                </c:if>
                <c:if test="${user.languageId==1}">
                "emptyTable": "Немає жодних доступних записів",
                "info": "Показано від _START_ до _END_ із _TOTAL_ записів",
                "infoFiltered":   "",
                "infoEmpty": "Немає даних",
                "search": "Шукати:",
                "paginate": {
                    "previous": "Попередня",
                    "next": "Наступна",
                }
                </c:if>
                <c:if test="${user.languageId==2}">
                "emptyTable": "Нет никаких доступных записей",
                "info": "Показано от _START_ до _END_ с _TOTAL_ записей",
                "infoFiltered":   "",
                "infoEmpty": "Нету данных",
                "search": "Искать:",
                "paginate": {
                    "previous": "Предыдущая",
                    "next": "Следующая",
                }
                </c:if>
            }
        });
    } );
</script>
</body>
</html>
