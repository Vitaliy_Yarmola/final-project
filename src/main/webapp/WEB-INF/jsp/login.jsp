<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<head>
    <title>Login Form</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="style/st4.css"/>">
</head>
<body>

<div class="hero">
    <div id="box" class="form-box">
        <div class="button-box">
            <div id="btn"></div>
            <button type="button" class="toggle-btn" onclick="login()"><fmt:message key="login.change.button.login"/></button>
            <button type="button" class="toggle-btn" onclick="register()"><fmt:message key="login.change.button.register"/></button>
        </div>
        <form id="login" class="input-group" action="controller" method="post">
            <label>
                <input name="email" type="text" class="input-field" placeholder="<fmt:message key="login.form.login.email"/>" required>
            </label>
            <label>
                <input name="password" type="password" class="input-field" placeholder="<fmt:message key="login.form.register.password"/>" required>
            </label>
            <input type="hidden" name="command" value="login"/>
            <input class="submit-btn btn-top" type="submit" value="<fmt:message key="login.form.login.button"/>"/>
        </form>
        <form id="register" class="input-group" action="controller" method="post">
            <label>
                <input name="email" id="email" type="text" class="input-field" placeholder="<fmt:message key="login.form.register.email"/>" onkeyup="checkData()" required>
            </label>
            <label>
                <input name="name" id="name" type="text" class="input-field" placeholder="<fmt:message key="main.modal.user.rent.input.name.holder"/>" onkeyup="checkData()" required>
            </label>
            <label>
                <input name="surname" id="surname" type="text" class="input-field" placeholder="<fmt:message key="main.modal.user.rent.input.surname.holder"/>" onkeyup="checkData()" required>
            </label>
            <label>
                <input name="phone" id="phone" type="text" class="input-field" placeholder="<fmt:message key="main.modal.user.rent.input.phone.holder"/>" onkeyup="checkData()" required>
            </label>
            <label>
                <input name="password" id="password" type="password" class="input-field" placeholder="<fmt:message key="login.form.register.password"/>" onkeyup="checkData()" required>
            </label>
            <label>
                <input name="rpt-pass" id="password2" type="password" class="input-field" placeholder="<fmt:message key="login.form.register.password.rpt"/>" onkeyup="checkData()" required>
            </label>
            <input type="hidden" name="command" value="register"/>
            <input class="submit-btn btn-top" id="register-btn" type="submit" value="<fmt:message key="login.form.register.button"/>" disabled/>
        </form>
    </div>

</div>
<script>
    let x = document.getElementById("login");
    let y = document.getElementById("register");
    let z = document.getElementById("btn");
    let b = document.getElementById("box");

    function register() {
        b.style.height = "460px";
        x.style.left = "-400px";
        y.style.left = "50px";
        z.style.left = "110px";
        z.style.width = "130px"
    }

    function login() {
        b.style.height = "240px";
        x.style.left = "50px";
        y.style.left = "450px";
        z.style.left = "0";
        z.style.width = "110px"
    }

    function checkData() {
        const email = document.getElementById("email").value;
        const name = document.getElementById("name").value;
        const surname = document.getElementById("surname");
        const phone = document.getElementById("phone").value;
        const password = document.getElementById("password").value;
        const password2 = document.getElementById("password2").value;
        const register = document.getElementById("register-btn");

        if (name.length !== 0 && surname.length !== 0
            && phone.length !== 0 && email.length !== 0 && password===password2
            && password.length !== 0 && password2.length !== 0) {
            register.removeAttribute('disabled');
        } else {
            register.setAttribute('disabled', 'disabled');
        }
    }

</script>
</body>
</html>