<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<c:set var="title" value="Profile" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="profile-back text-center">

    <form action="controller" method="post">
        <input type="hidden" name="command" value="log_out"/>
        <input class="btn btn-danger btn-position" type="submit" id="log_out"
               value="<fmt:message key="profile.log.out"/>"/>
    </form>
    <div class="float-center" style="padding-top: 20px">
        <h3>${user.name} ${user.surname}</h3>
    </div>
    <div class="float-left" style="margin-right: 20px">
        <div class="text-position">
            <label class="text-size"><fmt:message key="user.control.modal.add.manager.label.email"/></label>
        </div>
        <div class="text-position">
            <label class="text-size"><fmt:message key="main.modal.user.rent.input.phone"/></label>
        </div>
        <div class="text-position">
            <label class="text-size"><fmt:message key="main.modal.user.rent.input.date"/></label>
        </div>
        <div class="text-position">
            <label class="text-size"><fmt:message key="main.modal.user.rent.input.id"/></label>
        </div>

        <div class="text-position">
            <label class="text-size"><fmt:message key="user.profile.have.debt"/></label>
        </div>
    </div>
    <div>
        <div class="text-position">
            <label>${user.email}</label>
        </div>
        <div class="text-position">
            <label>${user.phone}</label>
        </div>
        <div class="text-position">
            <c:if test="${user.dateBirth!=null}">
                <label>${user.dateBirth} </label>
            </c:if>
            <c:if test="${user.dateBirth==null}">
                <label><fmt:message key="user.profile.null"/></label>
            </c:if>
        </div>
        <div class="text-position">
            <c:if test="${user.idCode!=0}">
                <label>${user.idCode}</label>
            </c:if>
            <c:if test="${user.idCode==0}">
                <label><fmt:message key="user.profile.null"/></label>
            </c:if>
        </div>
        <div class="text-position">
            <label><fmt:message key="user.profile.have.debt.${user.debtor}"/></label>
        </div>
    </div>
    <div class="form-group" style="padding-top: 20px">
        <button type="button" class="btn btn-primary" data-toggle="modal" onclick="checkEditParams()"
                data-target="#editData">
            <fmt:message key="user.profile.edit.title"/>
        </button>
        <button type="button" class="btn btn-primary" style="margin-left: 20px" data-toggle="modal"
                data-target="#editPass" onclick="cleanForm()">
            <fmt:message key="user.profile.edit.password"/>
        </button>
    </div>
</div>

<div class="modal fade" id="editData" tabindex="-1" role="dialog"
     aria-labelledby="editInformation" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="controller" method="post">
                <div class="modal-header text-center">
                    <h5 class="info-color font-weight-bold" id="editInformation"><fmt:message key="user.profile.edit.title"/></h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="<fmt:message key="main.modal.close"/>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="edit-email" class="text-dark"><fmt:message key="user.control.modal.add.manager.label.email"/></label>
                    <input name="ps-email" type="email" id="edit-email" class="form-control mb-2">

                    <label for="edit-name" class="text-dark"><fmt:message key="main.modal.user.rent.input.name"/></label>
                    <input name="ps-name" type="text" id="edit-name" class="form-control mb-2">

                    <label for="edit-surname" class="text-dark"><fmt:message key="main.modal.user.rent.input.surname"/></label>
                    <input name="ps-surname" type="text" id="edit-surname" class="form-control mb-2">

                    <label for="change-code" class="text-dark"><fmt:message key="main.modal.user.rent.input.id"/></label>
                    <input name="id-code" type="number" id="change-code" class="form-control mb-2">

                    <label for="edit-phone" class="text-dark"><fmt:message key="main.modal.user.rent.input.phone"/></label>
                    <input name="phone" type="text" id="edit-phone" class="form-control mb-2">

                    <div class="form-group">
                        <label for="change-date" class="text-dark"><fmt:message key="main.modal.user.rent.input.date"/></label>
                        <input name="ps-date" id="change-date" type="date"
                               class="form-control mb-2">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                    </button>
                    <input type="hidden" name="command" value="change_data"/>
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="user.profile.edit.change"/>"/>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editPass" tabindex="-1" role="dialog"
     aria-labelledby="changePass" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="pass-change" action="controller" method="post">
                <div class="modal-header text-center">
                    <h5 class="info-color font-weight-bold" id="changePass"><fmt:message key="user.profile.edit.password"/></h5>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="<fmt:message key="main.modal.close"/>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="current-pass" class="text-dark"><fmt:message key="user.profile.edit.password.current"/></label>
                    <input name="current-pass" type="password" id="current-pass"
                           class="form-control mb-2" onkeyup="checkPassword()"
                           placeholder="<fmt:message key="user.profile.edit.password.current.input"/>">

                    <label for="new-pass" class="text-dark"><fmt:message key="user.profile.edit.password.new"/></label>
                    <input name="new-pass" type="password" id="new-pass"
                           class="form-control mb-2" onkeyup="checkPassword()"
                           placeholder="<fmt:message key="user.profile.edit.password.new.input"/>">

                    <label for="rpt-pass" class="text-dark"><fmt:message key="user.profile.edit.password.repeat"/></label>
                    <input name="rpt-pass" type="password" id="rpt-pass"
                           class="form-control mb-2" onkeyup="checkPassword()"
                           placeholder="<fmt:message key="user.profile.edit.password.repeat.input"/>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                    </button>
                    <input type="hidden" name="command" value="change_password"/>
                    <input class="btn btn-primary" type="submit" id="change"
                           value="<fmt:message key="user.profile.edit.change"/>" disabled/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function checkEditParams() {
        $('#edit-email').val('${user.email}');
        $('#edit-name').val('${user.name}');
        $('#edit-surname').val('${user.surname}');
        $('#change-code').val('${user.idCode}');
        $('#edit-phone').val('${user.phone}');
        $('#change-date').val('${user.dateBirth}');
    }

    function checkPassword() {
        const current = $('#current-pass').val();
        const newPass = $('#new-pass').val();
        const rptPass = $('#rpt-pass').val();

        if (current.length !== 0 && newPass.length !== 0
            && rptPass.length !== 0 && newPass === rptPass) {
            $('#change').removeAttr('disabled');
        } else {
            $('#change').attr('disabled', 'disabled');
        }
    }

    function cleanForm() {
        document.getElementById("pass-change").reset();
    }
</script>
</body>
</html>
