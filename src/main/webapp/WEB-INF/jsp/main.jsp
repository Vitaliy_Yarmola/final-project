<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<c:set var="title" value="CAR RENT" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="table-light col col-lg-10" style="margin-left:auto; margin-right: auto">
    <div class="form_inline" style="padding: 10px">
        <div class="form-inline float-left">
            <form class="form-group" action="controller" method="get">
                <div class="input-group">
                    <select class="custom-select" name="inputGroup">
                        <c:forEach var="brands" items="${brands}">
                            <option value="${brands.name}">${brands.name}</option>
                        </c:forEach>
                    </select>
                    <div class="input-group-append">
                        <input type="hidden" name="command" value="get_brand"/>
                        <input class="btn btn-outline-primary" type="submit" value="<fmt:message key="main.show"/>"/>
                    </div>
                </div>
            </form>
            <form class="form-group" action="controller" method="get" style="padding-left: 10px">
                <div class="input-group">
                    <select class="custom-select" name="inputClasses">
                        <c:forEach var="classes" items="${classes}">
                            <option value="${classes.name}"><fmt:message key="car.class.${classes.name}"/></option>
                        </c:forEach>
                    </select>
                    <div class="input-group-append">
                        <input type="hidden" name="command" value="get_class"/>
                        <input class="btn btn-outline-primary" type="submit" value="<fmt:message key="main.show"/>"/>
                    </div>
                </div>
            </form>
            <c:if test="${user.roleId==0}">
                <form class="form-group" action="controller" method="get" style="padding-left: 10px">
                    <input type="hidden" name="command" value="get_deleted"/>
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="main.show_deleted"/>"/>
                </form>
            </c:if>
        </div>
        <div class="form-inline float-right">
            <c:if test="${user.roleId==0}">
                <div class="form-group" style="padding-right: 10px">
                    <button type="button" class="btn btn-primary btnModal float-left" data-toggle="modal"
                            data-target="#addModal">
                        <fmt:message key="main.add_new_car"/>
                    </button>
                </div>
            </c:if>
            <form class="form-group" style="padding-right: 10px" action="controller" method="get">
                <input type="hidden" name="command" value="main"/>
                <input class="btn btn-outline-primary" type="submit" value="<fmt:message key="main.sort_name"/>"/>
            </form>
            <div class="dropdown form-group" style="padding-right: 10px">
                <button class="btn btn-outline-primary dropdown-toggle" type="button"
                        id="dropSort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <fmt:message key="main.sort_price"/>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropSort">
                    <form action="controller" method="get">
                        <input name="sort" type="hidden" value="up">
                        <input type="hidden" name="command" value="sort_price"/>
                        <input class="dropdown-item" type="submit" value="<fmt:message key="main.sort.up"/>"/>
                    </form>
                    <form action="controller" method="get">
                        <input name="sort" type="hidden" value="down">
                        <input type="hidden" name="command" value="sort_price"/>
                        <input class="dropdown-item" type="submit" value="<fmt:message key="main.sort.down"/>"/>
                    </form>
                </div>
            </div>
            <form class="form-group" action="controller" method="post">
                <div class="input-group">
                    <input name="search" type="text" class="form-control"
                           placeholder="<fmt:message key="main.search_holder"/>"
                           aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <input type="hidden" name="command" value="search"/>
                        <input class="btn btn-outline-primary" type="submit" value="<fmt:message key="main.search"/>"/>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table id="tableWithCars" class="table table-striped table-bordered table-sm text-center"
           cellspacing="0"
           width="100%">
        <thead>
        <tr>
            <th class="th-sm"><fmt:message key="main.table.picture"/></th>
            <th class="th-sm"><fmt:message key="main.table.brand"/></th>
            <th class="th-sm"><fmt:message key="main.table.model"/></th>
            <th class="th-sm"><fmt:message key="main.table.class"/></th>
            <th class="th-sm"><fmt:message key="main.table.color"/></th>
            <th class="th-sm"><fmt:message key="main.table.capacity"/></th>
            <th class="th-sm"><fmt:message key="main.table.fuel"/></th>
            <th class="th-sm"><fmt:message key="main.table.gear_box"/></th>
            <th class="th-sm"><fmt:message key="main.table.price"/></th>
            <th class="th-sm"><fmt:message key="main.table.actions"/></th>
        </tr>
        </thead>
        <tbody id="myTable">
        <c:forEach var="cars" items="${cars}">
            <tr class>
                <td>
                    <a class="logo" id="pop${cars.id}" onclick="onPhotoClick('imageRes${cars.id}')">
                        <img id="imageRes${cars.id}" src="${cars.url}" alt="Photo">
                    </a>
                </td>
                <td>${cars.brand}</td>
                <td>${cars.model}</td>
                <td><fmt:message key="car.class.${cars.carcase}"/></td>
                <td><fmt:message key="car.color.${cars.color}"/></td>
                <td>${cars.capacity}</td>
                <td><fmt:message key="car.fuel.${cars.fuel}"/></td>
                <td><fmt:message key="car.gear.box.${cars.gearBox}"/></td>
                <td>${cars.price}</td>
                <td>
                    <!-- Button trigger modal -->
                    <c:if test="${user==null || user.roleId==2}">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                <c:if test="${user!=null}"> data-target="#rentModal" onclick="getValueOnClick('${cars.id}')"</c:if>
                                <c:if test="${user==null}">data-target="#warning"</c:if>>
                            <fmt:message key="main.table.buttons.rent"/>
                        </button>
                    </c:if>
                    <c:if test="${user.roleId==0 && cars.statusId!=2}">
                        <form action="controller" method="post">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#editModal" onclick="getParams(${cars.id}, '${cars.brand}',
                                        '${cars.model}', '${cars.carcase}', '${cars.color}',  '${cars.capacity}',
                                        '${cars.gearBox}', '${cars.fuel}', '${cars.price}')">
                                    <fmt:message key="main.table.buttons.edit"/>
                                </button>
                                <c:if test="${cars.statusId!=1}">
                                    <input name="car_number" type="hidden" value="${cars.id}">
                                    <input type="hidden" name="command" value="delete_car"/>
                                    <input class="btn btn-danger" type="submit"
                                           value="<fmt:message key="main.table.buttons.remove"/>"/>
                                </c:if>
                            </div>
                        </form>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<c:if test="${user==null || user.roleId==2}">
    <%@ include file="/WEB-INF/jspf/user_rent.jspf" %>
</c:if>

<c:if test="${user.roleId==0}">
    <%@ include file="/WEB-INF/jspf/admin_edit.jspf" %>
    <%@ include file="/WEB-INF/jspf/admin_add.jspf" %>
</c:if>

<div class="modal fade" id="warning" role="dialog">
    <div class="modal-dialog">
        <div class="alert alert-warning alert-dismissible">
            <a class="close" data-dismiss="modal" aria-label="close">&times;</a>
            <strong><fmt:message key="main.modal.warning.text"/> </strong> <fmt:message
                key="main.modal.warning.information"/>
        </div>
    </div>
</div>

<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"><fmt:message key="main.modal.preview"/></h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only"><fmt:message key="main.modal.close"/></span></button>
            </div>
            <div class="modal-body text-center">
                <img src="" id="imagePreview" style="width: 740px; height: auto;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message
                        key="main.modal.close"/></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function onPhotoClick(id) {
        const value = document.getElementById(id);
        $('#imagePreview').attr('src', $(value).attr('src'));
        $('#imageModal').modal('show');
    }

    $("#photo").change(function () {
        $("#uploadLabel").html(this.value);
    });
    $("#photo_add").change(function () {
        $("#uploadLabel_add").html(this.value);
    });

    //block
    function checkParams() {
        const name = $('#ps-name').val();
        const surname = $('#ps-surname').val();
        const date = $('#ps-date').val();
        const number = $('#phone').val();
        const id = $('#id-code').val();
        const date1 = $('#date1').val();
        const date2 = $('#date2').val();

        if (name.length !== 0 && surname.length !== 0
            && number.length !== 0 && id.length !== 0 && date.length !== 0
            && date1.length !== 0 && date2.length !== 0) {
            $('#create').removeAttr('disabled');
        } else {
            $('#create').attr('disabled', 'disabled');
        }
    }

    function getValueOnClick(car_id) {
        $('#carId').val(car_id);
        $('#ps-name').val('${user.name}');
        $('#ps-surname').val('${user.surname}');
        $('#phone').val('${user.phone}');
        <c:if test="${user.idCode!=null}">
        $('#ps-date').val('${user.dateBirth}');
        $('#id-code').val('${user.idCode}');
        </c:if>
    }

    function getParams(id, brand_val, model_val, case_val, color_val,
                       capacity_val, gear_val, fuel_val, price_val) {
        $('#car_number').val(id);
        $('#brand').val(brand_val);
        $('#model').val(model_val);
        $('#carcase').val(case_val);
        $('#color').val(color_val);
        $('#capacity').val(capacity_val);
        $('#gearBox').val(gear_val);
        $('#fuel').val(fuel_val);
        $('#price').val(price_val);
    }

    $(document).ready(function () {
        $('#tableWithCars').DataTable({
            searching: false, paging: true, info: true, "lengthChange": false,
            "lengthMenu": [[6], [6]],
            "aoColumnDefs": [
                {"bSortable": false, "aTargets": [0, 9]},
                {"bSearchable": false, "aTargets": [0, 9]}
            ],
            "language": {
                <c:if test="${user.languageId==0}">
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "Showing 0 to 0 of 0 entries",
                "paginate": {
                    "previous": "Previous page",
                    "next": "Next",
                }
                </c:if>
                <c:if test="${user.languageId==1}">
                "emptyTable": "Немає жодних доступних файлів",
                "info": "Показано від _START_ до _END_ із _TOTAL_ доступних автомобілів",
                "infoEmpty": "Немає даних",
                "paginate": {
                    "previous": "Попередня",
                    "next": "Наступна",
                }
                </c:if>
                <c:if test="${user.languageId==2}">
                "emptyTable": "Нет никаких доступных записей",
                "info": "Показано от _START_ до _END_ с _TOTAL_ записей",
                "infoEmpty": "Нету данных",
                "paginate": {
                    "previous": "Предыдущая",
                    "next": "Следующая",
                }
                </c:if>
            }
        });
    });
</script>
</body>
</html>
