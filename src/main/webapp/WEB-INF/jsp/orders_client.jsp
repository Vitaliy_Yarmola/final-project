<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>
<c:set var="title" value="Orders" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="table-back">
    <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <table id="ordersClient" class="table table-striped table-bordered table-sm text-center"
               cellspacing="0" width="100%" style="padding: 10px 10px 10px">
            <thead>
            <tr>
                <th class="th-sm"><fmt:message key="orders.admin.table.column.brand"/></th>
                <th class="th-sm"><fmt:message key="orders.admin.table.column.model"/></th>
                <th class="th-sm"><fmt:message key="orders.admin.table.column.cost"/></th>
                <th class="th-sm"><fmt:message key="orders.admin.table.column.driver"/></th>
                <th class="th-sm"><fmt:message key="orders.admin.table.column.start.rent"/></th>
                <th class="th-sm"><fmt:message key="orders.admin.table.column.end.rent"/></th>
                <th class="th-sm"><fmt:message key="orders.admin.table.column.status"/></th>
                <th class="th-sm"><fmt:message key="users.control.table.column.actions"/></th>
            </tr>

            </thead>
            <tbody>
            <c:forEach var="orders" items="${orders}">
                <tr>
                    <td>${orders.brand}</td>
                    <td>${orders.model}</td>
                    <td>${orders.cost}</td>
                    <td><fmt:message key="order.admin.driver.${orders.driver}"/></td>
                    <td>${orders.startRent}</td>
                    <td>${orders.endRent}</td>
                    <td><fmt:message key="order.admin.status.id.${orders.orderStatusId}"/></td>
                    <td>
                        <c:if test="${orders.orderStatusId==0}">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#applyModal"
                                    onclick="sendData(${orders.orderId}, '${orders.brand}', '${orders.model}', '${orders.cost}',
                                            '${orders.driver}',  '${orders.startRent}', '${orders.endRent}')">
                                <fmt:message key="orders.users.table.button.confirm"/>
                            </button>
                        </c:if>
                        <c:if test="${orders.orderStatusId==3}">
                            <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                    data-target="#infoModal" onclick="sendMessage('${orders.message}')">
                                <fmt:message key="orders.users.table.button.info"/>
                            </button>
                        </c:if>
                        <c:if test="${orders.orderStatusId==5}">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#payModal"
                                    onclick="sendCost(${orders.orderId}, '${orders.repair}', '${orders.message}')">
                                <fmt:message key="orders.users.table.button.damage.pay"/>
                            </button>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="applyModal" tabindex="-1" role="dialog"
     aria-labelledby="modalApply" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="controller" method="post">
                <div class="modal-header text-center">
                    <h5 class="info-color font-weight-bold" id="modalApply"><fmt:message key="orders.users.modal.apply.title"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="<fmt:message key="main.modal.close"/>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="order_id" id="order_num" type="hidden" value="0">
                    <input name="btnId" type="hidden" value="btn1">
                    <div class="float-left">
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.users.modal.apply.text.brand"/></label>
                        </div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.users.modal.apply.text.modal"/></label>
                        </div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.users.modal.apply.text.cost"/></label>
                        </div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.admin.table.column.driver"/>:</label>
                        </div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.admin.table.column.start.rent"/>:</label>
                        </div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.admin.table.column.end.rent"/>:</label>
                        </div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.users.modal.apply.text.bill"/></label>
                        </div>
                    </div>
                    <div>
                        <div class="text-position">
                            <label id="brand"></label>
                        </div>
                        <div class="text-position">
                            <label id="model"></label>
                        </div>
                        <div class="text-position">
                            <label id="cost"></label>
                        </div>
                        <div class="text-position">
                            <label id="driver"></label>
                        </div>
                        <div class="text-position">
                            <label id="start"></label>
                        </div>
                        <div class="text-position">
                            <label id="end"></label>
                        </div>
                        <div class="text-position">
                            <label>UA12329783272982012</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                    </button>
                    <input type="hidden" name="command" value="client_order"/>
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="orders.admin.table.button.apply"/>"/>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="infoModal" tabindex="-1" role="dialog"
     aria-labelledby="modalInfo" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="info-color font-weight-bold" id="modalInfo"><fmt:message key="orders.users.modal.info.title"/></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="<fmt:message key="main.modal.close"/>">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="payModal" tabindex="-1" role="dialog"
     aria-labelledby="modalPay" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="controller" method="post">
                <div class="modal-header text-center">
                    <h5 class="info-color font-weight-bold" id="modalPay"><fmt:message key="orders.users.modal.damage.pay.title"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="order_id" id="order_id" type="hidden" value="0">
                    <input name="btnId" type="hidden" value="btn2">
                    <div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.users.modal.damage.pay.message"/></label>
                            <label id ="repair_message"></label>
                        </div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.users.modal.damage.pay.cost"/></label>
                            <label id="cost_value"></label>
                            <label> UAH</label>
                        </div>
                        <div class="text-position">
                            <label class="text-size"><fmt:message key="orders.users.modal.apply.text.bill"/></label>
                            <label>UA12329783272982012</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                    </button>
                    <input type="hidden" name="command" value="client_order"/>
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="orders.admin.table.button.apply"/>"/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function sendData(id, brand, model, cost, driver, start, end) {
        document.getElementById("order_num").value = id;
        document.getElementById("brand").textContent = brand;
        document.getElementById("model").textContent = model;
        document.getElementById("cost").textContent = cost;
        document.getElementById("driver").textContent = driver;
        document.getElementById("start").textContent = start;
        document.getElementById("end").textContent = end;
    }

    function sendMessage(value) {
        document.getElementById("message").textContent = value;
    }

    function sendCost(id, value, message) {
        document.getElementById("order_id").value = id;
        document.getElementById("cost_value").textContent = value;
        document.getElementById("repair_message").textContent = message;
    }
</script>
</body>
</html>
