<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<c:set var="title" value="Home" scope="page"/>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="general-back text-center" style="top: 15%">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 col-md-8 align-center">
                <h3 class="mbr-section-subtitle align-center mbr-fonts-style pb-3 display-5" style="color: white">
                    <fmt:message key="general.h3.text"/></h3>
                <h2 class="mbr-section-title align-center mbr-fonts-style mbr-bold display-1" style="color: white">CAR RENT</h2>
            </div>
        </div>
    </div>
</div>
</body>
</html>
