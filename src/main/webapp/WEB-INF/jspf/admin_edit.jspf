<%@ page pageEncoding="UTF-8" %>
<%--Modal window for admin (edit information about cars)--%>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog"
     aria-labelledby="modalEdit" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="controller" enctype="multipart/form-data" id="car-edit" method="post">
                <div class="modal-header text-center">
                    <h5 class="info-color font-weight-bold" id="modalEdit"><fmt:message key="main.modal.admin.edit.title"/></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="<fmt:message key="main.modal.close"/>">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="car_number" id="car_number" type="hidden" value="0">
                    <div>
                        <label for="brand" class="text-dark"><fmt:message key="main.modal.admin.edit.label.brand"/></label>
                        <input name="brand" type="text" id="brand" class="form-control mb-2" onkeyup="checkEditData()"
                               placeholder="<fmt:message key="main.modal.admin.edit.input.brand"/>" list="list-brands">
                        <datalist id="list-brands">
                            <c:forEach var="brands" items="${brands}">
                                <option value="${brands.name}">
                            </c:forEach>
                        </datalist>
                    </div>
                    <div>
                        <label for="model" class="text-dark"><fmt:message key="main.modal.admin.edit.label.model"/></label>
                        <input name="model" type="text" id="model" class="form-control mb-2"
                               placeholder="<fmt:message key="main.modal.admin.edit.input.model"/>" onkeyup="checkEditData()">
                    </div>
                    <div>
                        <label for="carcase" class="text-dark"><fmt:message key="main.modal.admin.edit.label.class"/></label>
                        <select class="form-control mb-2 custom-select" name="carcase" id="carcase">
                            <c:forEach var="classes" items="${classes}">
                                <option value="${classes.name}"><fmt:message key="car.class.${classes.name}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                    <div>
                        <label for="color" class="text-dark"><fmt:message key="main.modal.admin.edit.label.color"/></label>
                        <select class="form-control mb-2 custom-select" name="color" id="color">
                            <option value="black"><fmt:message key="car.color.black"/></option>
                            <option value="blue"><fmt:message key="car.color.blue"/></option>
                            <option value="green"><fmt:message key="car.color.green"/></option>
                            <option value="beige"><fmt:message key="car.color.beige"/></option>
                            <option value="red"><fmt:message key="car.color.red"/></option>
                            <option value="purple"><fmt:message key="car.color.purple"/></option>
                            <option value="orange"><fmt:message key="car.color.orange"/></option>
                            <option value="yellow"><fmt:message key="car.color.yellow"/></option>
                            <option value="white"><fmt:message key="car.color.white"/></option>
                            <option value="brown"><fmt:message key="car.color.brown"/></option>
                            <option value="gray"><fmt:message key="car.color.gray"/></option>
                        </select>
                    </div>
                    <div>
                        <label for="capacity" class="text-dark"><fmt:message key="main.modal.admin.edit.label.engine"/></label>
                        <input name="capacity" type="number" id="capacity" class="form-control mb-2"
                               value="1.0" step="0.1" onkeyup="checkEditData()">
                    </div>
                    <div>
                        <label for="gearBox" class="text-dark"><fmt:message key="main.modal.admin.edit.label.gearBox"/></label>
                        <select class="form-control mb-2 custom-select" name="gearBox" id="gearBox">
                            <option value="mechanical"><fmt:message key="car.gear.box.mechanical"/></option>
                            <option value="automatic"><fmt:message key="car.gear.box.automatic"/></option>
                            <option value="robotic"><fmt:message key="car.gear.box.robotic"/></option>
                            <option value="variator"><fmt:message key="car.gear.box.variator"/></option>
                        </select>
                    </div>
                    <div>
                        <label for="fuel" class="text-dark"><fmt:message key="main.modal.admin.edit.label.fuel"/></label>
                        <select class="form-control mb-2 custom-select" name="fuel" id="fuel">
                            <option value="benzine"><fmt:message key="car.fuel.benzine"/></option>
                            <option value="diesel"><fmt:message key="car.fuel.diesel"/></option>
                            <option value="hybrid"><fmt:message key="car.fuel.hybrid"/></option>
                            <option value="gas"><fmt:message key="car.fuel.gas"/></option>
                            <option value="electric"><fmt:message key="car.fuel.electric"/></option>
                            <option value="propane"><fmt:message key="car.fuel.propane"/></option>
                            <option value="hydrogen"><fmt:message key="car.fuel.hydrogen"/></option>
                            <option value="methanol"><fmt:message key="car.fuel.methanol"/></option>
                            <option value="ethanol"><fmt:message key="car.fuel.ethanol"/></option>
                            <option value="biodiesel"><fmt:message key="car.fuel.biodiesel"/></option>
                        </select>
                    </div>
                    <div>
                        <label for="price" class="text-dark"><fmt:message key="main.modal.admin.edit.label.price"/></label>
                        <input name="price" type="number" id="price" class="form-control mb-2"
                               placeholder="<fmt:message key="main.modal.admin.edit.input.price"/>" step="0.1" onkeyup="checkEditData()">
                    </div>
                    <label class="text-dark"><fmt:message key="main.modal.admin.edit.label.picture"/></label>
                    <div class="custom-file">
                        <input name = "photo" type="file" class="custom-file-input" id="photo" lang="ua">
                        <label id="uploadLabel" class="custom-file-label" for="photo"><fmt:message key="main.modal.admin.edit.input.picture"/></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><fmt:message key="main.modal.close"/>
                    </button>
                    <input type="hidden" name="command" value="edit_car"/>
                    <input class="btn btn-primary" type="submit" id="edit_car" value="<fmt:message key="main.modal.admin.edit.button.edit"/>" disabled/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function checkEditData() {
        const brand = $('#brand').val();
        const model = $('#model').val();
        const capacity = $('#capacity').val();
        const price = $('#price').val();

        if (brand.length !== 0 && model.length !== 0
            && capacity.length !== 0 && price.length !== 0) {
            $('#edit_car').removeAttr('disabled');
        } else {
            $('#edit_car').attr('disabled', 'disabled');
        }
    }
</script>