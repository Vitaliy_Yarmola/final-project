-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: rent
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brands` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (0,'BMW'),(4,'Kia'),(3,'Nissan'),(5,'Renault'),(2,'Skoda'),(1,'Volkswagen');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_status`
--

DROP TABLE IF EXISTS `car_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `car_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_status`
--

LOCK TABLES `car_status` WRITE;
/*!40000 ALTER TABLE `car_status` DISABLE KEYS */;
INSERT INTO `car_status` VALUES (2,'deleted'),(0,'open'),(1,'used');
/*!40000 ALTER TABLE `car_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cars` (
  `id` int NOT NULL AUTO_INCREMENT,
  `picture_url` varchar(45) DEFAULT NULL,
  `brand` varchar(30) NOT NULL,
  `model` varchar(40) NOT NULL,
  `carcase` varchar(20) NOT NULL,
  `price` double NOT NULL,
  `color` varchar(30) NOT NULL,
  `engine_capacity` double NOT NULL,
  `gear_box` varchar(20) NOT NULL,
  `fuel` varchar(20) NOT NULL,
  `status_id` int NOT NULL,
  PRIMARY KEY (`id`,`status_id`),
  KEY `fk_cars_car_status1_idx` (`status_id`),
  CONSTRAINT `fk_cars_car_status1` FOREIGN KEY (`status_id`) REFERENCES `car_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES (1,'/uploads/picture1.jpg','BMW','X5','J-class',200,'red',3,'automatic','benzine',0),(2,'/style/img/car2.jpg','Volkswagen','Touareg','J-class',2800,'white',3,'automatic','benzine',0),(3,'/uploads/picture3.jpg','Volkswagen','Polo','D-class',120,'brown',2,'automatic','benzine',0),(4,'/uploads/picture4.jpeg','BMW','M5','D-class',1000,'blue',70,'automatic','benzine',0),(5,'/uploads/picture5.jpg','Skoda','Octavia','D-class',800,'gray',2,'automatic','benzine',0),(6,'/uploads/picture6.jpg','Skoda','Octavia','D-class',600,'gray',2,'automatic','benzine',0),(7,'/uploads/picture7.jpg','Nissan','Leaf','C-class',1000,'gray',80,'automatic','electric',0),(8,'/uploads/picture8.jpg','Kia','ProCeed','S-class',1200,'white',2,'automatic','benzine',0),(9,'/uploads/picture9.jpg','Renault','Sandero','J-class',1200,'red',5,'robotic','benzine',0),(10,'/uploads/picture10.jpg','Volkswagen','Golf 7','C-class',800,'gray',1.6,'automatic','diesel',0);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (0,'A-class'),(1,'B-class'),(2,'C-class'),(3,'D-class'),(4,'E-class'),(5,'F-class'),(7,'J-class'),(6,'M-class'),(8,'S-class');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (0,'English'),(2,'Русский'),(1,'Українська');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_status`
--

LOCK TABLES `order_status` WRITE;
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
INSERT INTO `order_status` VALUES (5,'Car demage'),(4,'Car returned'),(1,'Check'),(7,'Completed'),(6,'Damage pay'),(0,'Order pay'),(2,'Performed'),(3,'Rejected');
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status_id` int NOT NULL,
  `cost` double NOT NULL,
  `driver` tinyint NOT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(25) NOT NULL,
  `ID_code` int NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `begin_rent` datetime NOT NULL,
  `end_rent` datetime NOT NULL,
  `message` varchar(200) DEFAULT NULL,
  `repair` double DEFAULT NULL,
  `user_id` int NOT NULL,
  `car_id` int NOT NULL,
  `date_birth` date DEFAULT NULL,
  PRIMARY KEY (`id`,`status_id`,`user_id`,`car_id`),
  KEY `fk_orders_clients1_idx` (`user_id`),
  KEY `fk_orders_cars1_idx` (`car_id`),
  KEY `fk_orders_statuses1_idx` (`status_id`),
  CONSTRAINT `fk_orders_cars1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`),
  CONSTRAINT `fk_orders_clients1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_orders_statuses1` FOREIGN KEY (`status_id`) REFERENCES `order_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,3,2000,1,'Vitaliy','Yarmola',1234567890,'+380971523412','2020-10-17 18:00:00','2020-10-21 10:00:00',NULL,0,1,1,'2001-08-02'),(3,3,4000,1,'Sergii','Petrenko',1234567123,'+380681232311','2022-11-12 00:00:00','2022-12-12 00:00:00',NULL,NULL,7,1,'1978-11-12'),(5,7,6000,1,'Vitaliy','Yarmola',1234567890,'+380971523412','2020-10-18 00:00:00','2020-10-23 00:00:00',NULL,NULL,1,2,'2002-08-20'),(6,7,5760,0,'Vitaliy','Yarmola',1234567890,'+380971523412','2020-10-12 00:00:00','2020-10-14 00:00:00',NULL,2000,1,3,'2002-08-20'),(7,7,10000,1,'Sergii','Petrenko',1234567812,'+380681232311','2020-10-27 00:00:00','2020-11-03 00:00:00','Зломаний бампер!',4000,7,1,'2001-10-02'),(8,7,6000,1,'Sergii','Petrenko',484788781,'+380681232311','2020-10-12 00:00:00','2020-11-20 00:00:00',NULL,NULL,7,1,'1985-10-07'),(9,7,12000,1,'Sergii','Petrenko',484788781,'+380681232311','2020-10-24 00:00:00','2020-10-30 00:00:00',NULL,NULL,7,1,'1985-10-07'),(10,7,6000,0,'Vitaliy','Yarmola',1234567890,'+380971523412','2020-10-26 10:00:00','2020-10-31 10:00:00',NULL,NULL,1,1,'2002-08-20'),(11,7,3000,1,'Vitaliy','Yarmola',1234567890,'+380971523412','2020-11-10 10:00:00','2020-11-13 10:00:00',NULL,NULL,1,4,'2001-08-20'),(12,7,5600,0,'Vitaliy','Yarmola',1234567890,'+380971523412','1000-10-31 10:10:00','2020-10-26 10:10:00',NULL,NULL,1,8,'2001-08-20'),(13,7,12000,1,'Vitaliy','Yarmola',1234567890,'+380971523412','2020-10-26 10:10:00','2020-10-31 10:01:00',NULL,NULL,1,8,'2001-08-20'),(14,7,8000,1,'Sergii','Petrenko',484788781,'+380681232311','2020-10-20 10:10:00','2020-10-23 10:10:00','Зломаний бампер! Ремонт коштуватиме 1000грн',1000,7,7,'1985-10-07'),(15,7,2400,1,'Vitaliy','Yarmola',1234567890,'+380971523413','2020-10-26 12:00:00','2020-10-29 12:00:00','Вдарене праве крило!',3000,1,4,'2001-08-20'),(16,7,2800,1,'Vitaliy','Yarmola',1234567890,'+380971523413','2020-10-26 12:00:00','2020-10-28 12:00:00',NULL,NULL,1,4,'2001-08-20'),(17,7,3000,0,'Vitaliy','Yarmola',1234567890,'+380971523413','2020-10-28 10:00:00','2020-10-31 10:00:00','Зламане заднє сидіння',1000,1,7,'2001-08-20'),(18,7,1680,1,'Vitaliy','Yarmola',1234567890,'+380971523413','2020-11-02 10:00:00','2020-11-04 10:10:00','Машина зламана',1500,1,6,'2001-08-20');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (0,'administrator'),(2,'client'),(1,'manager');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_status`
--

DROP TABLE IF EXISTS `user_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_status`
--

LOCK TABLES `user_status` WRITE;
/*!40000 ALTER TABLE `user_status` DISABLE KEYS */;
INSERT INTO `user_status` VALUES (1,'locked'),(0,'unlocked');
/*!40000 ALTER TABLE `user_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(25) NOT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(25) NOT NULL,
  `role_id` int NOT NULL,
  `debtor` tinyint NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `ID_code` int DEFAULT NULL,
  `date` date DEFAULT NULL,
  `language_id` int NOT NULL,
  `status_id` int NOT NULL,
  PRIMARY KEY (`id`,`role_id`,`language_id`,`status_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_roles1_idx` (`role_id`),
  KEY `fk_users_languages1_idx` (`language_id`),
  KEY `fk_users_user_status1_idx` (`status_id`),
  CONSTRAINT `fk_users_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  CONSTRAINT `fk_users_roles1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `fk_users_user_status1` FOREIGN KEY (`status_id`) REFERENCES `user_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'yarmola13578@gmail.com','qwerty13','Vitaliy','Yarmola',2,0,'+380971523413',1234567890,'2001-08-20',2,0),(2,'admin@gmail.com','admin12ad','Vitaliy','Yarmola',0,0,'+380992312311',17849849,'2002-08-20',1,0),(6,'qweqwe12@gmail.com','asd123','Сергій','Писаренко',2,0,'+390671231311',1234567234,'1978-02-11',0,0),(7,'qwe123@gmail.com','qwe123','Sergii','Petrenko',2,0,'+380681232311',484788781,'1987-08-12',0,0),(8,'qwe12qwe@gmail.com','asd123','Anton','Vashchuk',1,0,'+380991232122',0,NULL,1,0),(9,'qwer123@gmail.com','qwe123','Andrii','Mysiychuk',2,0,'+380978634567',NULL,NULL,0,0),(10,'manager123@gmail.com','фів123','Микола','Первозванний',1,0,'+380975313243',NULL,NULL,0,0),(11,'user12@gmail.com','фівапр12','Дмитро','Пасічний',2,0,'+380978123127',NULL,NULL,1,0),(12,'ylslena2003@gmail.com','Дутф14','Олена','Ярмола',2,0,'0982693553',NULL,NULL,0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-26  2:13:30
